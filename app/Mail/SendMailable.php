<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->status = $message['status'];
        $this->data = $message['data'];
        $this->type = $message['type'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $status = $this->status;

        return $this->view('email.mail')->with([
            'status' => $status,
            'type' => $this->type,
            'data' => $this->data,
        ]);
    }
}
