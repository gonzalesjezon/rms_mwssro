<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'sections';

    protected $fillable = [
    	'name'
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
