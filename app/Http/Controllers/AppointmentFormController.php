<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Appointment;
use App\AppointmentForm;
use App\AppointmentRequirement;
use App\Job;
use App\JobOffer;
use App\Evaluation;
use App\Assumption;
use App\OathOffice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentFormController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Form');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-form.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $jobOffer = Evaluation::where('recommended',1)->getModels();

        return view('appointment-form.create')->with([
            'joboffers' => $jobOffer,
            'action' => 'AppointmentFormController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $form = new AppointmentForm;
        $form->fill($request->all());
        $form->form_status = ($request->form_status) ? 1 : 0;
        $form->created_by = Auth::id();
        $form->save();
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            // $requirement = $this->saveRequirement($request);
        return redirect()
            ->route('appointment-form.edit',[
                'form' => $form
            ])->with('success', 'Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = AppointmentForm::find($id);

        return view('appointment-form.edit',[
            'form' => $form
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = AppointmentForm::find($id);
        $form->fill($request->all());
        $form->form_status = ($request->form_status) ? 1 : 0;
        $form->updated_by = Auth::id();
        $form->save();
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            // $requirement = $this->saveRequirement($request);
        return redirect()
            ->route('appointment-form.edit',[
                'form' => $form
            ])->with('success', 'Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentForm::destroy($id);
        return redirect('/appointment-form')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){
        $applicant = AppointmentForm::find($request->id)->first();
        $numberInWord = $this->convert_number_to_words(@$applicant->applicant->job->plantilla_item->basic_salary);
        return view('appointment-form.report',[
            'applicant' => $applicant,
            'number_in_word' => $numberInWord
        ]);
    }

}
