<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AcceptanceResignation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AcceptanceResignationController extends Controller
{
    public function __construct()
    {
        View::share('title', 'Acceptance and Resignation');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $resignation = AcceptanceResignation::latest()
        ->paginate($perPage);

        return view('acceptance_resignation.index', [
            'resignations' => $resignation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applicants = Applicant::where('active',1)
        ->orderBy('last_name','asc')
        ->getModels();

         return view('acceptance_resignation.create',[
            'applicants' => $applicants
         ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'applicant_id' => 'required',
            'letter_date' => 'required',
            'resignation_date' => 'required'
        ]);

        $resignation = new AcceptanceResignation();
        $resignation->fill($request->all());
        $resignation->created_by = Auth::id();
        $resignation->save();

        $applicants = Applicant::where('active',1)
        ->orderBy('last_name','asc')
        ->getModels();

        return redirect()
            ->route('acceptance_resignation.edit',[
                'resignation' => $resignation,
            ])
            ->with('success', 'The acceptance of resignation was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcceptanceResignation  $acceptanceResignation
     * @return \Illuminate\Http\Response
     */
    public function show(AcceptanceResignation $acceptanceResignation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcceptanceResignation  $acceptanceResignation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resignation = AcceptanceResignation::find($id);

        $applicants = Applicant::where('active',1)
        ->orderBy('last_name','asc')
        ->getModels();

        return view('acceptance_resignation.edit',[
            'resignation' => $resignation,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcceptanceResignation  $acceptanceResignation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $resignation = AcceptanceResignation::find($request->id);
        $resignation->fill($request->all());
        $resignation->updated_by = Auth::id();
        $resignation->save();

        return redirect()
            ->route('acceptance_resignation.edit',[
                'resignation' => $resignation,
            ])
            ->with('success', 'The acceptance of resignation was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcceptanceResignation  $acceptanceResignation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AcceptanceResignation::destroy($id);
        return redirect('acceptance_resignation')->with('success', 'Acceptance of resignation record deleted!');
    }

    public function report(Request $request){

        $resignation = AcceptanceResignation::find($request->id)->first();

        return view('acceptance_resignation.report',[
            'resignation' => $resignation
        ]);
    }
}
