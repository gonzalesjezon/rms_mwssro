<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AppointmentProcessing;
use App\AppointmentIssued;
use App\Appointment;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\AppointmentChecklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentProcessingController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Processing Checklist');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentProcessing::latest()
            ->paginate($perPage);

        return view('appointment-processing.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointments = AppointmentIssued::getModels();

        return view('appointment-processing.create')->with([
            'appointments' => $appointments,
            'action' => 'AppointmentProcessingController@store',
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appointment = new AppointmentProcessing;
        $appointment->fill($request->all());
        $appointment->educ_check = ($request->educ_check) ? 1 : 0;
        $appointment->exp_check = ($request->exp_check) ? 1 : 0;
        $appointment->training_check = ($request->training_check) ? 1 : 0;
        $appointment->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $appointment->other_check = ($request->other_check) ? 1 : 0;
        $appointment->created_by = Auth::id();

        $appointment->save();


        return redirect()
            ->route('appointment-processing.edit',[
                'processing' => $appointment
            ])->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $processing = AppointmentProcessing::find($id);
        
        return view('appointment-processing.edit',[
            'processing' => $processing
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $appointment = AppointmentProcessing::find($id);
        $appointment->fill($request->all());
        $appointment->educ_check = ($request->educ_check) ? 1 : 0;
        $appointment->exp_check = ($request->exp_check) ? 1 : 0;
        $appointment->training_check = ($request->training_check) ? 1 : 0;
        $appointment->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $appointment->other_check = ($request->other_check) ? 1 : 0;
        $appointment->updated_by = Auth::id();

        $appointment->save();

        return redirect()
            ->route('appointment-processing.edit',[
                'processing' => $appointment
            ])->with('success', 'The Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentProcessing::destroy($id);
        return redirect('/appointment-processing')->with('success', 'Appointment Processing successfully deleted!');
    }

    public function report(Request $request){

        $appointment = AppointmentProcessing::find($request->id);

        return view('appointment-processing.report')->with([
            'appointment' => $appointment,
        ]);
    }



}
