<?php

namespace App\Http\Controllers;

use App\AppointmentForm;
use App\AppointmentIssued;
use App\AppointmentProcessing;
use App\Appointment;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppointmentIssuedController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Appointment Issued');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $issues = AppointmentIssued::latest()
            ->paginate($perPage);

        return view('appointment-issued.index', [
            'issues' => $issues
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $issues = AppointmentForm::where('form_status',1)->getModels();

        return view('appointment-issued.create')->with([
            'issues' => $issues,
            'action' => 'AppointmentIssuedController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $form = new AppointmentIssued;
        $form->fill($request->all());
        $form->created_by = Auth::id();
        $form->save();
        return redirect()
            ->route('appointment-issued.edit',[
                'form' => $form
            ])->with('success', 'The Transmital was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function show(AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = AppointmentIssued::find($id);

        return view('appointment-issued.edit',[
            'form' => $form,
        ])->with('success','Apppointment issue successfully created.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $issued = AppointmentIssued::find($id);
        $issued->fill($request->all());
        $issued->updated_by = Auth::id();
        $issued->save();
        return redirect()
            ->route('appointment-issued.edit',[
                'form' => $form
            ])->with('success', 'The appointment issued was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentIssued::destroy($id);
        return redirect('/appointment-issued')->with('success', 'appointment issued data deleted!');
    }

}
