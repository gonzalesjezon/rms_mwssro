<?php

namespace App\Http\Controllers;

use App\OathOffice;
use App\Applicant;
use App\AppointmentRequirement;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OathOfficeController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Oath of Office');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $oathoffice = OathOffice::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);

        return view('oath-office.index', [
            'oathoffices' => $oathoffice
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boardings = AppointmentRequirement::getModels();

        return view('oath-office.create')->with([
            'boardings' => $boardings,
            'action' => 'OathOfficeController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oathoffice = new OathOffice;
        $oathoffice->fill($request->all());
        $oathoffice->created_by = Auth::id();
        $oathoffice->save();

        return redirect()
            ->route('oath-office.edit',[
                'oathoffice' => $oathoffice
            ])
            ->with('success','Oath of Office successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function show(OathOffice $oathOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oathoffice = OathOffice::find($id);

        return view('oath-office.edit')->with([
            'oathoffice' => $oathoffice,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oathoffice = OathOffice::find($id);
        $oathoffice->fill($request->all());
        $oathoffice->updated_by = Auth::id();
        $oathoffice->save();

        return redirect()
            ->route('oath-office.edit',[
                'oathoffice' => $oathoffice
            ])
            ->with('success','Oath of Office successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OathOffice::destroy($id);
        return redirect('/oath-office')->with('success', 'Oath of Office was successfully deleted.');
    }

    public function oathOfficeReport(Request $request){

        $oathoffice = OathOffice::find($request->id);

        return view('oath-office.report',[
            'oathoffice' => $oathoffice,
        ]);
    }
}
