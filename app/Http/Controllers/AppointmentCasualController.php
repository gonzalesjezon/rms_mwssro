<?php

namespace App\Http\Controllers;

use App\AppointmentCasual;
use App\Applicant;
use App\AppointmentRequirement;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;

class AppointmentCasualController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Plantilla of Casual Appointment');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentCasual::latest()
            ->paginate($perPage);

        return view('appointment-casual.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $boards = AppointmentRequirement::getModels();

        return view('appointment-casual.create')->with([
            'boards' => $boards,
            'action' => 'AppointmentCasualController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $casual = new AppointmentCasual;
        $casual->fill($request->all());
        $casual->created_by = Auth::id();
        $casual->save();

        return redirect()
            ->route('appointment-casual.edit',[
                'casual' => $casual
            ])->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $casual = AppointmentCasual::find($id);

        return view('appointment-casual.edit',[
            'casual' => $casual
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $casual = AppointmentCasual::find($id);
        $casual->fill($request->all());
        $casual->created_by = Auth::id();
        $casual->save();

        return redirect()
            ->route('appointment-casual.edit',[
                'casual' => $casual
            ])->with('success', 'The Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentCasual::destroy($id);
        return redirect('/appointment-casual')->with('success', 'Appointment data deleted!');
    }
}
