<?php

namespace App\Http\Controllers;

use App\AppointmentRequirement;
use App\Applicant;
use App\PositionDescription;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PositionDescriptionController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Position Description');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $boards = PositionDescription::latest()
            ->paginate($perPage);

        return view('position-descriptions.index', [
            'boards' => $boards
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $boardings = AppointmentRequirement::getModels();

        return view('position-descriptions.create')->with([
            'boardings' => $boardings,
            'action' => 'PositionDescriptionController@store',
            'option' => config('params.option_1')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $position_desc = new PositionDescription;

        $position_desc->fill($request->all());
        $position_desc->created_by = Auth::id();
        $position_desc->save();
        return redirect()
            ->route('position-descriptions.edit',[
                'posDesc' => $position_desc
            ])
            ->with('success', 'The Postion Description was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function show(PositionDescription $positionDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $posDesc = PositionDescription::find($id);
        
        return view('position-descriptions.edit',[
            'posDesc' => $posDesc,
            'option' => config('params.option_1')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position_desc = PositionDescription::find($id);
        $position_desc->fill($request->all());
        $position_desc->updated_by = Auth::id();
        $position_desc->save();
        return redirect()
            ->route('position-descriptions.edit',[
                'posDesc' => $position_desc
            ])
            ->with('success', 'The Postion Description was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PositionDescription::destroy($id);
        return redirect('/position-descriptions')->with('success', 'Position description data deleted!');
    }

    public function posDescriptionReport(Request $request)
    {
        $pos_desc = new PositionDescription;
        if($request->id){
            $pos_desc = PositionDescription::find($request->id);
        }
        return view('position-descriptions.report',[
            'pos_desc' => $pos_desc
        ]);
    }
}
