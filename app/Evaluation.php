<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evaluations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'job_id',
        'performance_average',
        'performance_percent',
        'performance_score',
        'education_points',
        'graduate_points',
        'education_graduate_total_points',
        'education_graduate_percent',
        'education_graduate_score',
        'education_training_percent',
        'education_training_score',
        'experience_points',
        'number_of_year_points',
        'experience_total_points',
        'experience_percent',
        'experience_score',
        'potential_r1',
        'potential_r2',
        'potential_r3',
        'potential_r4',
        'potential_total_points',
        'potential_percent',
        'potential_score',
        'psychosocial_r1',
        'psychosocial_r2',
        'psychosocial_r3',
        'psychosocial_r4',
        'psychosocial_total_points',
        'psychosocial_percent',
        'psychosocial_score',
        'examination_total_points',
        'examination_percent',
        'examination_score',
        'total_percent',
        'total_score',
        'evaluated_by',
        'reviewed_by',
        'noted_by',
        'recommended',
    ];

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant','applicant_id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
