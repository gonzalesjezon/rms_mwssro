<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatrixQualification extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'matrix_qualifications';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'age',
        'education',
        'experience',
        'eligibility',
        'training',
        'remarks',
        'isc_chairperson',
        'isc_member_one',
        'isc_member_two',
        'ea_representative',
    ];

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    /**
     * Relation: one is to one
     * Matrix has one Evaluation
     * @return mixed
     *
     */
    public function evaluation()
    {
        return $this->hasOne('App\Evaluation', 'applicant_id', 'applicant_id');
    }
}
