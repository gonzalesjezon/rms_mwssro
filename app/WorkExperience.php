<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'workexperiences';
    protected $fillable = [

		'applicant_id',
		'inclusive_date_from',
		'inclusive_date_to',
		'position_title',
		'department',
		'monthly_salary',
		'salary_grade',
		'status_of_appointment',
		'govt_service'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
