<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'empstatus';
}
