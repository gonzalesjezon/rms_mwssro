<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('evaluations')) {
            Schema::create('evaluations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('applicant_id');
                $table->integer('performance', false, true)->nullable();
                $table->integer('performance_divide', false, true)->nullable();
                $table->integer('performance_average', false, true)->nullable();
                $table->integer('performance_percent', false, true)->nullable();
                $table->integer('performance_score', false, true)->nullable();
                $table->string('eligibility')->nullable();
                $table->string('training')->nullable();
                $table->string('seminar')->nullable();
                $table->integer('minimum_education_points', false, true)->nullable();
                $table->integer('minimum_training_points', false, true)->nullable();
                $table->integer('education_points', false, true)->nullable();
                $table->integer('training_points', false, true)->nullable();
                $table->integer('education_training_total_points', false, true)->nullable();
                $table->integer('education_training_percent', false, true)->nullable();
                $table->integer('education_training_score', false, true)->nullable();
                $table->string('relevant_positions_held')->nullable();
                $table->integer('minimum_experience_requirement', false, true)->nullable();
                $table->integer('additional_points', false, true)->nullable();
                $table->integer('experience_accomplishments_total_points', false, true)->nullable();
                $table->integer('experience_accomplishments_percent', false, true)->nullable();
                $table->integer('experience_accomplishments_score', false, true)->nullable();
                $table->integer('potential', false, true)->nullable();
                $table->decimal('potential_average_rating', 5, 2)->nullable();
                $table->integer('potential_percentage_rating', false, true)->nullable();
                $table->integer('potential_percent', false, true)->nullable();
                $table->integer('potential_score', false, true)->nullable();
                $table->integer('psychosocial', false, true)->nullable();
                $table->integer('psychosocial_average_rating', false, true)->nullable();
                $table->integer('psychosocial_percentage_rating', false, true)->nullable();
                $table->integer('psychosocial_percent', false, true)->nullable();
                $table->integer('psychosocial_score', false, true)->nullable();
                $table->integer('total_percent', false, true)->nullable();
                $table->decimal('total_score', 5, 2)->nullable();
                $table->integer('evaluated_by')->nullable();
                $table->integer('reviewed_by')->nullable();
                $table->integer('noted_by')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
