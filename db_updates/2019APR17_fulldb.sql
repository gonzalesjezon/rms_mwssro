-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rms_mwssro
DROP DATABASE IF EXISTS `rms_mwssro`;
CREATE DATABASE IF NOT EXISTS `rms_mwssro` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rms_mwssro`;

-- Dumping structure for table rms_mwssro.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `remarks`, `gwa`, `zip_code`, `permanent_zip_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '', 'Patrick Lester', 'N.', 'Ty', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(2, 2, '', 'Darrell John', 'S.', 'Magsambol', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(3, 3, '', 'Leo James', 'B.', 'Abaloyan', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(4, 4, '', 'Renato ', 'L.', 'Rodriguez', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(5, 5, '', 'Lamberto ', 'M.', 'Talplacido', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(6, 6, '', 'Karl Mark', 'N.', 'Zapanta', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(7, 7, '', 'Ma. Karmela', 'B.', 'Dizon', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(8, 9, '', 'Lorna ', 'C.', 'Medina', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(9, 10, '', 'Evelyn ', 'B.', 'Agustin', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(10, 12, '', 'Mario ', 'G.', 'Macatangay', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(11, 13, '', 'Ma. Carla', 'N.', 'Benito', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(12, 14, '', 'Manuelito ', 'M.', 'Aparato', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(13, 15, '', 'Maria Eloisa', 'A.', 'Co', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(14, 17, '', 'Joel ', 'A.', 'Dominguez', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(15, 22, '', 'Claudine ', 'B.', 'Orocio-Isorena', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(16, 23, '', 'Ramon ', 'A.', 'Javier', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(17, 24, '', 'Virginia ', 'V.', 'Octa', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(18, 25, '', 'Yolanda ', 'C.', 'Vicente', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(19, 26, '', 'Maria Theresa', 'V.', 'Makiling', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(20, 27, '', 'Alan ', 'D.', 'Chuegan', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(21, 28, '', 'Christian Bernard', 'D.', 'Marcelino', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(22, 29, '', 'Roberto ', 'U.', 'Coloso', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(23, 30, '', 'John Oliver', 'S.', 'Medina', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(24, 31, '', 'Crescenciano B.', 'Minas', 'Jr.', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(25, 33, '', 'Angela Sigrid', 'J.', 'Along', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(26, 34, '', 'Manuel ', 'D.', 'Gacula', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(27, 37, '', 'Emelita ', 'M.', 'Romero', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(28, 38, '', 'Christopher ', 'D.', 'Chuegan', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(29, 40, '', 'Rosalinda ', 'T.', 'Valdez', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(30, 41, '', 'Justine Irish', 'C.', 'Ignacio', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(31, 42, '', 'Mark Billy', 'B.', 'Antonio', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(32, 43, '', 'Victor John', 'G.', 'Dizon', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(33, 44, '', 'Emyl Angelique', 'Fulgueras', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(34, 45, '', 'Maria Sharlene', 'P.', 'Zausa', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(35, 46, '', 'Mary Ann Monic', 'M.', 'Peña', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(36, 47, '', 'Charmaine Shiela', 'R.', 'Abia', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(37, 48, '', 'Candelaria ', 'P.', 'Castasus', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(38, 49, '', 'Lee Robert', 'M.', 'Britanico', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(39, 50, '', 'Steve ', 'P.', 'Leido', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(40, 51, '', 'Rosendo ', 'O.', 'Alegre', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(41, 52, '', 'Kimberly ', 'O.', 'Soliven', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(42, 53, '', 'Melchor ', 'S.', 'Cordova', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(43, 54, '', 'Olivia ', 'I.', 'Tolentino', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(44, 55, '', 'Carlito ', 'E.', 'Espallardo', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(45, 56, '', 'Lorna ', 'C.', 'Balingit', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(46, 57, '', 'Noel ', 'D.', 'Gappi', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(47, 58, '', 'Ma. Victoria', 'M.', 'Villarba', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(48, 59, '', 'Francis Eduardo', 'P.', 'Ayapana', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(49, 60, '', 'Vincent A.', 'Ruelos', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(50, 61, '', 'Joriel M.', 'Dagsa', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(51, 63, '', 'Christine Agatha', 'R.', 'Villanueva', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(52, 64, '', 'Klea Rejoice', 'D.', 'Luz', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(53, 65, '', 'Vicente ', 'T.', 'Avila III', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(54, 66, '', 'Edgar ', 'G.', 'Lumbres', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(55, 67, '', 'Mark ', 'B.', 'Tabes', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(56, 68, '', 'Roberto ', 'A.', 'Diala', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(57, 69, '', 'Crisanto ', 'G.', 'Nagtalon', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(58, 70, '', 'Ranjev ', 'M.', 'Garcia', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL),
	(59, 71, '', 'Sir Gil', 'P.', 'Maravilla', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-17 18:26:20', NULL, NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_casual: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_checklist
DROP TABLE IF EXISTS `appointment_checklist`;
CREATE TABLE IF NOT EXISTS `appointment_checklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) DEFAULT NULL,
  `form34b_cscfo` varchar(225) DEFAULT NULL,
  `form212_cscfo` varchar(225) DEFAULT NULL,
  `eligibility_cscfo` varchar(225) DEFAULT NULL,
  `form1_cscfo` varchar(225) DEFAULT NULL,
  `form32_cscfo` varchar(225) DEFAULT NULL,
  `form4_cscfo` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_checklist: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_checklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_checklist` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `publication_assessment_date` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `deliberation_date` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_forms: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_issued: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_processing: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_path` varchar(225) DEFAULT NULL,
  `saln_path` varchar(225) DEFAULT NULL,
  `eligibility_path` varchar(225) DEFAULT NULL,
  `training_path` varchar(225) DEFAULT NULL,
  `psa_path` varchar(225) DEFAULT NULL,
  `tor_path` varchar(225) DEFAULT NULL,
  `diploma_path` varchar(225) DEFAULT NULL,
  `philhealth_path` varchar(225) DEFAULT NULL,
  `nbi_path` varchar(225) DEFAULT NULL,
  `medical_path` varchar(225) DEFAULT NULL,
  `bir_path` varchar(225) DEFAULT NULL,
  `coe_path` varchar(225) DEFAULT NULL,
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.appointment_requirements: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.assumptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.attestations: ~0 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.boarding_applicants
DROP TABLE IF EXISTS `boarding_applicants`;
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.config: ~0 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.countries: ~0 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.departments: ~0 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'PUBLIC INFORMATION DEPARTMENT', NULL, NULL, NULL, NULL, NULL),
	(2, 'OPERATIONS MONITORING DEPARTMENT', NULL, NULL, NULL, NULL, NULL),
	(3, 'WATER QUALITY CONTROL DEPARTMENT', NULL, NULL, NULL, NULL, NULL),
	(4, 'Administration Department', NULL, NULL, NULL, NULL, NULL),
	(5, 'LEGAL DEPARTMENT', NULL, NULL, NULL, NULL, NULL),
	(6, 'TARIFF CONTROL AND MONITORING DEPT.', NULL, NULL, NULL, NULL, NULL),
	(7, 'FINANCIAL AUDIT & ASSET MONITORING DEPT.', NULL, NULL, NULL, NULL, NULL),
	(8, 'COMPLAINTS SERVICES MONITORING DEPT.', NULL, NULL, NULL, NULL, NULL),
	(9, 'METERING EFFICIENCY DEPARTMENT', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.divisions
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.divisions: ~0 rows (approximately)
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.educations: ~0 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` varchar(225) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.eligibilities: ~0 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.employee_information
DROP TABLE IF EXISTS `employee_information`;
CREATE TABLE IF NOT EXISTS `employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `hired_date` timestamp NULL DEFAULT NULL,
  `assumption_date` timestamp NULL DEFAULT NULL,
  `resigned_date` timestamp NULL DEFAULT NULL,
  `rehired_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `position_item_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) NOT NULL,
  `work_schedule_id` int(11) NOT NULL,
  `appointment_status_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.employee_information: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_information` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance_average` int(10) unsigned DEFAULT NULL,
  `performance_percent` int(10) unsigned DEFAULT NULL,
  `performance_score` decimal(5,2) unsigned DEFAULT NULL,
  `education_points` int(10) unsigned DEFAULT NULL,
  `graduate_points` int(10) unsigned DEFAULT NULL,
  `education_graduate_total_points` int(10) unsigned DEFAULT NULL,
  `education_graduate_percent` int(10) unsigned DEFAULT NULL,
  `education_graduate_score` decimal(5,2) unsigned DEFAULT NULL,
  `education_training_percent` int(10) unsigned DEFAULT NULL,
  `education_training_score` decimal(5,2) unsigned DEFAULT NULL,
  `experience_points` int(10) unsigned DEFAULT NULL,
  `number_of_year_points` int(10) unsigned DEFAULT NULL,
  `experience_total_points` int(10) unsigned DEFAULT NULL,
  `experience_percent` int(10) unsigned DEFAULT NULL,
  `experience_score` decimal(5,2) unsigned DEFAULT NULL,
  `potential_r1` int(10) unsigned DEFAULT NULL,
  `potential_r2` int(10) unsigned DEFAULT NULL,
  `potential_r3` int(10) unsigned DEFAULT NULL,
  `potential_r4` int(10) unsigned DEFAULT NULL,
  `potential_total_points` int(10) unsigned DEFAULT NULL,
  `potential_percent` int(10) unsigned DEFAULT NULL,
  `potential_score` decimal(5,2) unsigned DEFAULT NULL,
  `psychosocial_r1` int(10) unsigned DEFAULT NULL,
  `psychosocial_r2` int(10) unsigned DEFAULT NULL,
  `psychosocial_r3` int(10) unsigned DEFAULT NULL,
  `psychosocial_r4` int(10) unsigned DEFAULT NULL,
  `psychosocial_total_points` int(10) unsigned DEFAULT NULL,
  `psychosocial_percent` int(10) unsigned DEFAULT NULL,
  `psychosocial_score` decimal(5,2) unsigned DEFAULT NULL,
  `examination_total_points` int(10) unsigned DEFAULT NULL,
  `examination_percent` int(10) unsigned DEFAULT NULL,
  `examination_score` decimal(5,2) unsigned DEFAULT NULL,
  `total_percent` int(10) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.examinations: ~0 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.interviews: ~0 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `psipop_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_assignment` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `psipop_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `status`, `place_of_assignment`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `expires`, `deadline_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 155030.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73811.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46008.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25232.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18125.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 109197.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(7, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(9, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21038.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 137195.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(11, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65319.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(12, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107444.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15859.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(14, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65319.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(15, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(16, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(17, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107444.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(18, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(19, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51155.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(20, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(21, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19233.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(22, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 137195.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(23, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70827.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(24, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120337.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(25, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(26, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55468.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(27, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53700.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(28, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41140.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(29, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36159.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(30, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15859.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(31, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107444.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(32, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(33, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73811.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(34, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57805.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(35, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57805.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(36, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 137195.000, 0.000, 2000.000, 6000.000, 137195.000, 137195.000, 5000.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-27', 1, 0, 0, 0, 0, '2019-04-17 18:50:08', '2019-04-17 10:47:10', 0, NULL, NULL),
	(37, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73157.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(38, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 109197.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(39, 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(40, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70827.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(41, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(42, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41140.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(43, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107444.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(44, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(45, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73157.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(46, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46008.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(47, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66385.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(48, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44294.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(49, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 137195.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(50, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73157.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(51, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120337.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(52, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(53, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73157.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(54, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(55, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107444.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(56, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15859.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(57, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65319.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(58, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44294.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(59, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70827.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(60, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41140.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(61, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58748.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(62, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30531.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(63, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25232.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(64, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22938.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(65, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16352.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(66, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16352.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(67, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15738.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(68, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16352.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(69, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16352.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(70, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40637.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL),
	(71, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30531.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-04-17 18:50:08', NULL, 0, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.job_offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.matrix_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.migrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.oath_offices: ~0 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.offices
DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.offices: ~0 rows (approximately)
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'OFFICE OF THE CHIEF REGULATOR', NULL, NULL, NULL, NULL, NULL),
	(2, 'OFFICE OF THE DEPUTY ADMINISTRATOR TECHNICAL REGULATION', NULL, NULL, NULL, NULL, NULL),
	(3, 'OFFICE OF THE DEPUTY ADMINISTRATOR FOR ADMINISTRATION AND LEGAL MATTERS', NULL, NULL, NULL, NULL, NULL),
	(4, 'OFFICE OF THE DEPUTY ADMINISTRATOR FOR FINANCIAL REGULATION', NULL, NULL, NULL, NULL, NULL),
	(5, 'OFFICE OF THE DEPUTY ADMINISTRATOR FOR  CUSTOMER SERVICE REGULATION ', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.position_descriptions
DROP TABLE IF EXISTS `position_descriptions`;
CREATE TABLE IF NOT EXISTS `position_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `immediate_supervisor` varchar(225) DEFAULT NULL,
  `higher_supervisor` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `used_tools` text,
  `managerial` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `non_supervisor` int(11) DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `general_public` int(11) DEFAULT NULL,
  `other_agency` varchar(50) DEFAULT NULL,
  `other_contacts` varchar(50) DEFAULT NULL,
  `office_work` int(11) DEFAULT NULL,
  `field_work` int(11) DEFAULT NULL,
  `other_condition` varchar(50) DEFAULT NULL,
  `description_function_unit` text,
  `description_function_position` text,
  `compentency_1` text,
  `compentency_2` text,
  `percentage_work_time` varchar(50) DEFAULT NULL,
  `responsibilities` varchar(50) DEFAULT NULL,
  `supervisor_name` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.position_descriptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `position_descriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `position_descriptions` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.preliminary_evaluation
DROP TABLE IF EXISTS `preliminary_evaluation`;
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.preliminary_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `employee_status` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `salary_grade` int(11) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `code` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL,
  `annual_authorized_salary` varchar(50) DEFAULT NULL,
  `annual_actual_salary` decimal(10,2) DEFAULT NULL,
  `ppa_attribution` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.psipop: ~0 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `applicant_id`, `employee_status`, `office_id`, `division_id`, `department_id`, `section_id`, `salary_grade`, `step`, `position_title`, `item_number`, `code`, `level`, `type`, `annual_authorized_salary`, `annual_actual_salary`, `ppa_attribution`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 3, 1, NULL, 0, NULL, 29, 1, 'Senior Deputy Administrator ', '01', NULL, NULL, NULL, '1860360', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(2, 2, 2, 1, NULL, 0, NULL, 23, 1, 'Head Technical Assistant ', '02', NULL, NULL, NULL, '885732', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(3, 3, 1, 1, NULL, 0, NULL, 19, 2, 'Management Information System Design Specialist A', '03', NULL, NULL, NULL, '552096', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(4, 4, 1, 1, NULL, 0, NULL, 13, 1, 'Data Analyst Controller', '04', NULL, NULL, NULL, '302784', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(5, 5, 1, 1, NULL, 0, NULL, 9, 1, 'Driver- Mechanic A     ', '05', NULL, NULL, NULL, '217500', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(6, 6, 1, 0, NULL, 1, NULL, 26, 1, 'Department Manager A  ', '06', NULL, NULL, NULL, '1310364', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(7, 7, 1, 0, NULL, 1, NULL, 7, 1, 'Secretary B ', '07', NULL, NULL, NULL, '188856', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(8, NULL, 0, 0, NULL, 1, NULL, 18, 1, 'Senior Information Officer ', '08', NULL, NULL, NULL, '487644', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(9, 8, 1, 0, NULL, 1, NULL, 11, 2, 'Artist Illustrator A', '09', NULL, NULL, NULL, '252456', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(10, 9, 3, 2, NULL, 0, NULL, 28, 1, 'Deputy Administrator ', '10', NULL, NULL, NULL, '1646340', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(11, NULL, 0, 2, NULL, 0, NULL, 22, 1, 'Technical Assistant A  ', '11', NULL, NULL, NULL, '783828', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(12, 10, 1, 0, NULL, 2, NULL, 26, 1, 'Department Manager A', '12', NULL, NULL, NULL, '1289328', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(13, 11, 1, 0, NULL, 2, NULL, 7, 2, 'Secretary B', '13', NULL, NULL, NULL, '190308', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(14, 12, 1, 0, NULL, 2, NULL, 22, 1, 'Supervising Water Utilities Regulation Officer', '14', NULL, NULL, NULL, '783828', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(15, 13, 1, 0, NULL, 2, NULL, 18, 1, 'Senior Water Utilities Regulation Officer', '15', NULL, NULL, NULL, '487644', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(16, NULL, 0, 0, NULL, 2, NULL, 18, 1, 'Senior Water Utilities Regulation Officer', '16', NULL, NULL, NULL, '487644', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(17, 14, 1, 0, NULL, 3, NULL, 26, 1, 'Department Manager A', '17', NULL, NULL, NULL, '1289328', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(18, NULL, 0, 0, NULL, 3, NULL, 7, 1, 'Secretary B', '18', NULL, NULL, NULL, '188856', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(19, NULL, 0, 0, NULL, 3, NULL, 20, 1, 'Principal Chemist', '19', NULL, NULL, NULL, '613860', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(20, NULL, 0, 0, NULL, 3, NULL, 18, 1, 'Senior Water Utilities Regulation Officer', '20', NULL, NULL, NULL, '487644', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(21, NULL, 0, 0, NULL, 3, NULL, 10, 1, 'Laboratory Technician', '21', NULL, NULL, NULL, '230796', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(22, 15, 3, 3, NULL, 0, NULL, 28, 1, 'Deputy Administrator', '22', NULL, NULL, NULL, '1646340', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(23, 16, 1, 3, NULL, 0, NULL, 22, 6, 'Technical Assistant A', '23', NULL, NULL, NULL, '849924', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(24, 17, 1, 0, NULL, 4, NULL, 26, 8, 'Department Manager A', '24', NULL, NULL, NULL, '1444044', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(25, 18, 1, 0, NULL, 4, NULL, 7, 1, 'Secretary B', '25', NULL, NULL, NULL, '188856', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(26, 19, 1, 0, NULL, 4, NULL, 20, 1, 'Finance Officer B', '26', NULL, NULL, NULL, '665616', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(27, 20, 1, 0, NULL, 4, NULL, 20, 1, 'Finance Officer B', '27', NULL, NULL, NULL, '644400', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(28, 21, 1, 0, NULL, 4, NULL, 18, 2, 'Senior Industrial Relation Management Officer A', '28', NULL, NULL, NULL, '493680', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(29, 22, 1, 0, NULL, 4, NULL, 16, 7, 'Senior Property Officer', '29', NULL, NULL, NULL, '433908', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(30, 23, 1, 0, NULL, 4, NULL, 7, 2, 'Driver Mechanic B', '30', NULL, NULL, NULL, '190308', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(31, 24, 1, 0, NULL, 5, NULL, 26, 1, 'Department Manager A', '31', NULL, NULL, NULL, '1289328', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(32, NULL, 1, 0, NULL, 5, NULL, 7, 1, 'Secretary B ', '32', NULL, NULL, NULL, '188856', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(33, 25, 1, 0, NULL, 5, NULL, 23, 1, 'Chief Corporate Attorney', '33', NULL, NULL, NULL, '885732', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(34, 26, 1, 0, NULL, 5, NULL, 21, 1, 'Senior Corporate Attorney', '34', NULL, NULL, NULL, '693660', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(35, NULL, 0, 0, NULL, 5, NULL, 21, 1, 'Senior Corporate Attorney', '35', NULL, NULL, NULL, '693660', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(36, NULL, 3, 4, NULL, 0, NULL, 28, 1, 'Deputy Administrator ', '36', NULL, NULL, NULL, '1646340', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(37, 27, 1, 4, NULL, 0, NULL, 22, 8, 'Technical Assistant A ', '37', NULL, NULL, NULL, '877884', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(38, 28, 1, 0, NULL, 6, NULL, 26, 1, 'Department Manager A', '38', NULL, NULL, NULL, '1310364', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(39, 29, 1, 0, NULL, 6, NULL, 22, 6, 'Chief Economist', '40', NULL, NULL, NULL, '849924', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(40, 30, 1, 0, NULL, 6, NULL, 18, 1, 'Senior Water Utilities Regulation Officer ', '41', NULL, NULL, NULL, '487644', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(41, 31, 1, 0, NULL, 6, NULL, 18, 2, 'Senior Water Utilities Regulation Officer', '42', NULL, NULL, NULL, '493680', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(42, 32, 1, 0, NULL, 7, NULL, 26, 1, 'Department Manager A  ', '43', NULL, NULL, NULL, '1289328', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(43, 33, 1, 0, NULL, 7, NULL, 7, 1, 'Secretary B', '44', NULL, NULL, NULL, '188856', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(44, 34, 1, 0, NULL, 7, NULL, 22, 8, 'Supervising Financial Management Specialist', '45', NULL, NULL, NULL, '877884', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(45, 35, 1, 0, NULL, 7, NULL, 19, 1, 'Finance Officer C ', '46', NULL, NULL, NULL, '552096', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(46, 36, 1, 0, NULL, 7, NULL, 22, 2, 'Supervising Water Utilities Regulation Officer', '47', NULL, NULL, NULL, '796620', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(47, 37, 1, 0, NULL, 7, NULL, 18, 8, 'Senior Water Utilities Regulation Officer ', '48', NULL, NULL, NULL, '531528', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(48, 38, 3, 5, NULL, 0, NULL, 28, 1, 'Deputy Administrator           ', '49', NULL, NULL, NULL, '1646340', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(49, 39, 1, 5, NULL, 0, NULL, 22, 8, 'Technical Assistant A', '50', NULL, NULL, NULL, '877884', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(50, 40, 1, 0, NULL, 8, NULL, 26, 8, 'Department Manager A', '51', NULL, NULL, NULL, '1444044', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(51, 41, 1, 0, NULL, 8, NULL, 7, 1, 'Secretary B', '52', NULL, NULL, NULL, '188856', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(52, 42, 1, 0, NULL, 8, NULL, 22, 8, 'Supervising Water Utilities Regulation Officer', '53', NULL, NULL, NULL, '877884', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(53, 43, 1, 0, NULL, 8, NULL, 18, 1, 'Senior Water Utilities Regulation Officer', '54', NULL, NULL, NULL, '487644', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(54, 44, 1, 0, NULL, 9, NULL, 26, 1, 'Department Manager A', '55', NULL, NULL, NULL, '1289328', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(55, 45, 1, 0, NULL, 9, NULL, 7, 2, 'Secretary B', '56', NULL, NULL, NULL, '190308', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(56, 46, 1, 0, NULL, 9, NULL, 22, 1, 'Supervising Water Utilities Regulation Officer', '57', NULL, NULL, NULL, '783828', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(57, 47, 1, 0, NULL, 9, NULL, 18, 8, 'Senior Water Utilities Regulation Officer', '58', NULL, NULL, NULL, '531528', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(58, 48, 1, 0, NULL, 9, NULL, 22, 6, 'Supervising Water Utilities Regulation Officer', '59', NULL, NULL, NULL, '849924', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(59, 49, 1, 0, NULL, 9, NULL, 18, 2, 'Senior Water Utilities Regulation Officer  ', '60', NULL, NULL, NULL, '493680', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(60, 50, 1, 0, NULL, 4, NULL, 21, 2, 'Chief Corporate Accountant A ', '61', NULL, NULL, NULL, '704976', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(61, NULL, 1, 0, NULL, 4, NULL, 15, 1, 'Industrial Relations Management Officer A', '62', NULL, NULL, NULL, '366372', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(62, 51, 1, 0, NULL, 4, NULL, 13, 1, 'Fiscal Examiner A  ', '63', NULL, NULL, NULL, '302784', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(63, 52, 1, 0, NULL, 4, NULL, 12, 1, 'Records Officer C', '64', NULL, NULL, NULL, '275256', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(64, 53, 1, 0, NULL, 4, NULL, 7, 6, 'Driver- Mechanic B', '65', NULL, NULL, NULL, '196224', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(65, 54, 1, 0, NULL, 4, NULL, 7, 6, 'Driver- Mechanic B', '66', NULL, NULL, NULL, '196224', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(66, 55, 1, 0, NULL, 4, NULL, 7, 1, 'Driver- Mechanic B ', '67', NULL, NULL, NULL, '188856', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(67, 56, 1, 0, NULL, 4, NULL, 7, 6, 'Driver- Mechanic B', '68', NULL, NULL, NULL, '196224', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(68, 57, 1, 0, NULL, 4, NULL, 7, 6, 'Driver- Mechanic B', '69', NULL, NULL, NULL, '196224', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(69, 58, 1, 0, NULL, 4, NULL, 18, 1, 'Administrative Officer III', '70', NULL, NULL, NULL, '487644', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(70, 59, 1, 0, NULL, 4, NULL, 15, 1, 'Administrative Officer II', '71', NULL, NULL, NULL, '366372', NULL, NULL, 1, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL),
	(71, NULL, 0, 0, NULL, 6, NULL, 7, 1, 'Secretary B', '39', NULL, NULL, NULL, '188856', NULL, NULL, 0, NULL, NULL, '2019-04-17 16:49:20', NULL, NULL);
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.recommendations
DROP TABLE IF EXISTS `recommendations`;
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `sign_one` varchar(225) DEFAULT NULL,
  `sign_two` varchar(225) DEFAULT NULL,
  `sign_three` varchar(225) DEFAULT NULL,
  `sign_four` varchar(225) DEFAULT NULL,
  `sign_five` varchar(225) DEFAULT NULL,
  `prepared_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.recommendations: ~0 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.sections: ~0 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.selection_lineup: ~0 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.trainings: ~0 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_mwssro.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin', NULL, '$2y$10$B.ZO79UiXJyLuY706Wepa.MVM3X8BjCE/ullOFJI7lhFLX1Fva.JG', 'dn2wH3kd5NntxMfKjFCbzgo5OxREVjb3bPnqwBKMH56nSVNyTMuzgCyyETHb', '2019-01-26 07:32:58', '2019-01-26 07:32:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table rms_mwssro.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` varchar(50) DEFAULT NULL,
  `inclusive_date_to` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_mwssro.workexperiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
