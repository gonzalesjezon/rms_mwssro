-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plantilla_item_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `professional_fees` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_assignment` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cos_position_title` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_specify` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `duration_from` date DEFAULT NULL,
  `duration_to` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `appointer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.jobs: ~2 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `plantilla_item_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `professional_fees`, `status`, `station`, `reporting_line`, `cos_position_title`, `other_specify`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `other_qualification`, `csc_education`, `csc_work_experience`, `csc_eligibility`, `csc_training`, `expires`, `publish_date`, `deadline_date`, `duration_from`, `duration_to`, `approved_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `appointer_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(4, 25, NULL, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'nonplantilla', NULL, NULL, NULL, NULL, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-14', NULL, NULL, NULL, 1, 1, 0, 0, 0, 1, '2019-08-31 08:06:49', '2019-08-31 08:20:31', 1, NULL, NULL),
	(5, 23, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 2000.000, 6000.000, 12674.000, 12674.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 'FB', NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-07', NULL, NULL, '2019-09-02', 1, 1, 0, 0, 1, 1, '2019-08-31 08:07:12', '2019-09-02 02:51:25', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table pcw_db.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_number` varchar(225) DEFAULT NULL,
  `incumbent_name` varchar(225) DEFAULT NULL,
  `mode_of_separation` varchar(225) DEFAULT NULL,
  `date_vacated` date DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `salary_grade_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `basic_salary` decimal(13,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.psipop: ~78 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `item_number`, `incumbent_name`, `mode_of_separation`, `date_vacated`, `position_id`, `office_id`, `division_id`, `department_id`, `employee_status_id`, `salary_grade_id`, `step_id`, `status`, `basic_salary`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(2, 'NCRFWB-EXED3-1-1998', 'VERZOSA, EMMELINE LAHOZ', NULL, NULL, 1, NULL, 1, NULL, 1, 27, 5, 1, 122031.00, NULL, NULL, NULL, NULL),
	(3, 'NCRFWB-DED3-1-1998', 'GUTIERREZ, CECILE BALTAZAR', NULL, NULL, 2, NULL, 1, NULL, 1, 26, 3, 1, 106091.00, NULL, NULL, NULL, NULL),
	(4, 'NCRFWB-DED3-1-2004', 'BALMES, MARIA KRISTINE JOSEFINA GONDA', NULL, NULL, 2, NULL, 1, NULL, 1, 26, 1, 1, 102910.00, NULL, NULL, NULL, NULL),
	(5, 'NCRFWB-CADOF-9-2004', 'ETRATA, LOLITA ESPINA', NULL, NULL, 3, NULL, 2, NULL, 1, 23, 1, 1, 73299.00, NULL, NULL, NULL, NULL),
	(6, 'NCRFWB-ATY3-1-2016', 'CUEVAS, MAY ZYRA VILLAS', NULL, NULL, 4, NULL, 1, NULL, 1, 20, 1, 1, 52554.00, NULL, NULL, NULL, NULL),
	(7, 'NCRFWB-ATY3-2-2016', 'SALDANA, BIANCA BALAJORO', NULL, NULL, 4, NULL, 1, NULL, 1, 20, 1, 1, 52554.00, NULL, NULL, NULL, NULL),
	(8, 'NCRFWB-A3-1-1998', 'CATUBAG, CHARY GRACE CAISIDO', NULL, NULL, 5, NULL, 2, NULL, 1, 18, 1, 1, 42099.00, NULL, NULL, NULL, NULL),
	(9, 'NCRFWB-ADOF5-10-2004', 'ALVIS, RICA BRAÃ‘A', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 8, 1, 41413.00, NULL, NULL, NULL, NULL),
	(10, 'NCRFWB-ADOF5-11-2004', 'TASONG, MARIA CALAMBA', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(11, 'NCRFWB-ADOF5-12-2004', 'CAASI, MARIA THERESA ESTELLA', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(12, 'NCRFWB-A2-1-1998', 'MANUEL, NATHANAEL ANTONIO', NULL, NULL, 7, 0, 0, 0, 1, 15, 1, 1, 31765.00, NULL, 1, NULL, '2019-08-31 07:23:16'),
	(13, 'NCRFWB-ADOF3-14-2004', 'MAANO, LEANDRA ARLENE ALBA', NULL, NULL, 8, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(14, 'NCRFWB-ADOF3-15-2004', 'PADAYAO, CLARITA CARAYUGAN', NULL, NULL, 8, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(15, 'NCRFWB-SADAS2-1-2004', 'RAMIREZ, LISA ROBEL', NULL, NULL, 9, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(16, 'NCRFWB-ADAS2-5-2004', 'OBUSAN, JOANN PAJARES', NULL, NULL, 10, NULL, 1, NULL, 1, 33, 1, 1, 16282.00, NULL, NULL, NULL, NULL),
	(17, 'NCRFWB-ADAS2-6-2004', 'PUNZALAN, MICAELA MORAL', NULL, NULL, 10, NULL, 2, NULL, 1, 33, 8, 1, 17369.00, NULL, NULL, NULL, NULL),
	(18, 'NCRFWB-ADAS2-7-2004', 'PAPASIN, EMORIE MUYOT', NULL, NULL, 10, NULL, 2, NULL, 1, 33, 8, 1, 17369.00, NULL, NULL, NULL, NULL),
	(19, 'NCRFWB-ADAS1-6-2004', 'SILVERIO, REY BALMACEDA', NULL, NULL, 11, NULL, 2, NULL, 1, 7, 2, 1, 15380.00, NULL, NULL, NULL, NULL),
	(20, 'NCRFWB-ADA6-3-2004', 'OCONNOR, BERNADETTE ROLDAN', NULL, NULL, 12, NULL, 2, NULL, 1, 6, 2, 1, 14459.00, NULL, NULL, NULL, NULL),
	(21, 'NCRFWB-ADA4-8-2004', NULL, NULL, NULL, 13, NULL, 2, NULL, 1, 4, 8, 0, 13424.00, NULL, NULL, NULL, NULL),
	(22, 'NCRFWB-ADA4-13-2004', 'GALLANO, ALBERT DINO', NULL, NULL, 13, NULL, 2, NULL, 1, 4, 5, 1, 13097.00, NULL, NULL, NULL, NULL),
	(23, 'NCRFWB-ADA4-14-2004', NULL, NULL, NULL, 72, 13, 1, 0, 1, 4, 1, 0, 12674.00, NULL, 1, NULL, '2019-08-31 08:03:51'),
	(24, 'NCRFWB-ADA4-12-2004', 'MADOLI, MA. RAESSA MASAGANDA', NULL, NULL, 13, NULL, 2, NULL, 1, 4, 1, 1, 12674.00, NULL, NULL, NULL, NULL),
	(25, 'NCRFWB-ADA4-11-2004', 'Jhon Doe', 'Resign', '2019-09-05', 238, 8, 14, 0, 2, 4, 1, 0, 12674.00, NULL, 1, NULL, '2019-09-05 04:38:43'),
	(26, 'NCRFWB-ADA3-15-2004', 'ABILAY, VIRGILIO GARIN', NULL, NULL, 14, NULL, 2, NULL, 1, 3, 8, 1, 12620.00, NULL, NULL, NULL, NULL),
	(27, 'NCRFWB-ADA3-16-2004', 'CAPIRAL, ENRIQUE LOPENA', NULL, NULL, 14, NULL, 2, NULL, 1, 3, 8, 1, 12620.00, NULL, NULL, NULL, NULL),
	(28, 'NCRFWB-ADA2-8-2004', 'QUIJANA, ARLYNE INACAY', NULL, NULL, 15, NULL, 2, NULL, 1, 2, 6, 1, 11671.00, NULL, NULL, NULL, NULL),
	(29, 'NCRFWB-INFO5-2-2007', 'CASTRO, HONEY MACAPAGAL', NULL, NULL, 16, NULL, 3, NULL, 1, 23, 1, 1, 73299.00, NULL, NULL, NULL, NULL),
	(30, 'NCRFWB-INFO4-3-2007', NULL, NULL, NULL, 17, NULL, 3, NULL, 1, 21, 2, 0, 59597.00, NULL, NULL, NULL, NULL),
	(31, 'NCRFWB-ITO1-6-2007', 'ATANACIO, VICKY TORRES', NULL, NULL, 18, NULL, 3, NULL, 1, 18, 2, 1, 42730.00, NULL, NULL, NULL, NULL),
	(32, 'NCRFWB-INFO3-1-2008', 'FRANCISCO, ANNE DOMINIQUE DELOS SANTOS', NULL, NULL, 19, NULL, 3, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(33, 'NCRFWB-SADAS3-2-2004', 'SOTTO, ANNA MARIA BAUTISTA', NULL, NULL, 20, NULL, 3, NULL, 1, 14, 2, 1, 29359.00, NULL, NULL, NULL, NULL),
	(34, 'NCRFWB-INFOSA2-7-2007', 'NATIVIDAD, NICO TAGLE', NULL, NULL, 21, NULL, 3, NULL, 1, 15, 2, 1, 32147.00, NULL, NULL, NULL, NULL),
	(35, 'NCRFWB-PLO2-4-2016', 'SANTOS, RAMIL PANGILINAN', NULL, NULL, 22, NULL, 3, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(36, 'NCRFWB-INFOSA1-6-2016', 'ESQUIBAL, MARY GLADYS ANTON', NULL, NULL, 23, NULL, 3, NULL, 1, 11, 1, 1, 22149.00, NULL, NULL, NULL, NULL),
	(37, 'NCRFWB-LIB1-1-2007', 'PASCUAL, JONATHAN ABELLO', NULL, NULL, 24, NULL, 3, NULL, 1, 10, 4, 1, 20963.00, NULL, NULL, NULL, NULL),
	(38, 'NCRFWB-PLO1-5-2016', 'VILLALUNA, MARICAR DULFO', NULL, NULL, 25, NULL, 3, NULL, 1, 10, 1, 1, 20179.00, NULL, NULL, NULL, NULL),
	(39, 'NCRFWB-ADAS3-5-2004', NULL, NULL, NULL, 26, NULL, 3, NULL, 1, 8, 1, 0, 17627.00, NULL, NULL, NULL, NULL),
	(40, 'NCRFWB-ADA6-5-2004', 'GARMINO, RICARDO JAVIER', NULL, NULL, 12, NULL, 3, NULL, 1, 6, 4, 1, 14699.00, NULL, NULL, NULL, NULL),
	(41, 'NCRFWB-CGAD-7-2016', 'MILLAR, NHARLEEN SANTOS', NULL, NULL, 27, NULL, 4, NULL, 1, 23, 3, 1, 75512.00, NULL, NULL, NULL, NULL),
	(42, 'NCRFWB-SVGAD-6-2016', 'SASUMAN, JOSEPHINE KHALEEN MOISES', NULL, NULL, 28, NULL, 4, NULL, 1, 21, 2, 1, 59597.00, NULL, NULL, NULL, NULL),
	(43, 'NCRFWB-SVGAD-7-2016', 'BAYLOSIS, MA. REBECCA RAFAELA REYES', NULL, NULL, 28, NULL, 4, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(44, 'NCRFWB-SRGAD-6-2016', 'BRIGOLA, MARIA JANICA VILLAS', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(45, 'NCRFWB-SRGAD-8-2016', 'SAN JUAN, CLEHENIA AURORA BETCO', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(46, 'NCRFWB-SRGAD-7-2016', 'AREVALO, AVERY SILK SORIANO', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(47, 'NCRFWB-GAD2-4-2016', 'LAGUMBAY, ANASTACIO MALAKI', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 6, 1, 30799.00, NULL, NULL, NULL, NULL),
	(48, 'NCRFWB-GAD2-8-2016', 'LEE, KRISTINE ANNE VIRGO', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(49, 'NCRFWB-GAD2-3-2016', 'ORCILLA, ARMANDO GOMEZ', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(50, 'NCRFWB-GAD2-6-2016', 'ABIOG, ANIKA HANNA BELUANG', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(51, 'NCRFWB-GAD2-5-2016', 'RAYA, VINNA ABEGAIL DARIO', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(52, 'NCRFWB-GAD2-7-2016', 'MALUYA, JENNYLIN NUNEZ', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(53, 'NCRFWB-PLA-1-1998', 'BUAN, RICHELLE CANETE', NULL, NULL, 31, NULL, 4, NULL, 1, 33, 1, 1, 16282.00, NULL, NULL, NULL, NULL),
	(54, 'NCRFWB-ADA4-17-2004', 'TIAUZON, JOMAY FERRER', NULL, NULL, 32, NULL, 4, NULL, 1, 4, 1, 1, 12674.00, NULL, NULL, NULL, NULL),
	(55, 'NCRFWB-CGAD-6-2016', 'BALEDA, ANITA ESTRERA', NULL, NULL, 27, NULL, 5, NULL, 1, 23, 2, 1, 74397.00, NULL, NULL, NULL, NULL),
	(56, 'NCRFWB-SVGAD-5-2016', 'DULAWAN, JEANETTE KINDIPAN', NULL, NULL, 28, NULL, 5, NULL, 1, 21, 3, 1, 60491.00, NULL, NULL, NULL, NULL),
	(57, 'NCRFWB-SVGAD-8-2016', 'MAZO, RAYMOND JAY LACSA', NULL, NULL, 28, NULL, 5, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(58, 'NCRFWB-SRGAD-5-2016', 'DAGNALAN, KAREN GASPI', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 3, 1, 39007.00, NULL, NULL, NULL, NULL),
	(59, 'NCRFWB-SRGAD-4-2016', 'TEODORO, KIMBERLY ANNE EFONDO', NULL, NULL, 29, NULL, 5, NULL, 1, 18, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(60, 'NCRFWB-SRGAD-10-2016', 'PEJI, KIM HAROLD TAMAYO', NULL, NULL, 29, NULL, 5, NULL, 1, 19, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(61, 'NCRFWB-SRGAD-9-2016', 'HUFANCIA, RONALYN VALENZUELA', NULL, NULL, 29, NULL, 5, NULL, 1, 20, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(62, 'NCRFWB-SRGAD-3-2016', 'BELLIN, JOCELYN TOLENTINO', NULL, NULL, 29, NULL, 5, NULL, 1, 21, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(63, 'NCRFWB-GAD2-2-2016', 'OMAS-AS, ELIZABETH BALAGOT', NULL, NULL, 30, NULL, 5, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(64, 'NCRFWB-GAD2-9-2016', 'PATIO, WRAKLE SANCHEZ', NULL, NULL, 30, NULL, 5, NULL, 1, 15, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(65, 'NCRFWB-GAD2-1-2016', 'MANGA, RHODORA ANGELINE VALDEZ', NULL, NULL, 30, NULL, 5, NULL, 1, 16, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(66, 'NCRFWB-ADA4-16-2004', NULL, NULL, NULL, 13, NULL, 5, NULL, 1, 4, 6, 0, 13206.00, NULL, NULL, NULL, NULL),
	(67, 'NCRFWB-CGAD-8-2016', 'JUSAYAN, MACARIO TORRES', NULL, NULL, 27, NULL, 6, NULL, 1, 23, 2, 1, 74397.00, NULL, NULL, NULL, NULL),
	(68, 'NCRFWB-SVGAD-9-2016', 'DELGADO, MARIANNE KRISTINE VITO', NULL, NULL, 28, NULL, 6, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(69, 'NCRFWB-SVGAD-10-2016', 'SUSARA, PAMELA CAPERIÃ‘A', NULL, NULL, 28, NULL, 6, NULL, 1, 22, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(70, 'NCRFWB-SRGAD-12-2016', 'CHUCK, SILAYAN TERESITA KINTANAR', NULL, NULL, 29, NULL, 6, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(71, 'NCRFWB-SRGAD-11-2016', 'CORRAL, MILDRED LATOGA', NULL, NULL, 29, NULL, 6, NULL, 1, 18, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(72, 'NCRFWB-GAD2-12-2016', 'ESTURAS, CLAIRE RUZZEL ALIBUDBUD', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(73, 'NCRFWB-GAD2-11-2016', 'GANDEZA JR., RENE ANDRADA', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(74, 'NCRFWB-GAD2-13-2016', 'RODRIGUEZ, BERNADETTE DIMACULANGAN', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(75, 'NCRFWB-LIB2-1-1998', NULL, NULL, NULL, 33, NULL, 3, NULL, 1, 14, 4, 0, 30071.00, NULL, NULL, NULL, NULL),
	(76, 'NCRFWB-GAD2-14-2016', NULL, NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 0, 29010.00, NULL, NULL, NULL, NULL),
	(77, 'NCRFWB-ADOF3-13-2004', NULL, NULL, NULL, 34, NULL, 2, NULL, 1, 13, 8, 0, 28759.00, NULL, NULL, NULL, NULL),
	(78, 'NCRFWB-ADOF5-3-2016', 'MAYRENA, ROSE JEAN M', NULL, NULL, 53, 7, NULL, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table pcw_db.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `notify_resched_exam` int(11) DEFAULT '0',
  `notify_exam_status` int(11) DEFAULT '0',
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `date_approved` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `vice` varchar(225) DEFAULT NULL,
  `who` varchar(225) DEFAULT NULL,
  `posted_in` varchar(50) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping structure for table ovp_hris.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
