@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="form-group row">
    {{ Form::label('applicant_name', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">

    @if(@$casual)

    {{ Form::label('applicant_name', $casual->applicant->getFullName() ,['class'=>'col-12 col-sm-2 col-form-label font-weight-bold']) }}

    <input type="hidden" name="applicant_id" value="{{$casual->applicant_id}}">

    @else

    <select class="form-control-sm form-control-xs" name="applicant_id" id="applicant_id" style="width: 100% !important">
        <option value="">Select Applicant</option>
        @foreach(@$boards as $board)
        <option value="{{ @$board->applicant_id }}" data-position="{{ @$board->applicant->job->plantilla_item->position->Name }}" data-office="{{ @$board->applicant->job->plantilla_item->office->Name }}" data-salary_grade="{{ @$board->applicant->job->plantilla_item->salary_grade->Name }}" data-item_number="{{ @$board->applicant->job->plantilla_item->item_number }}" data-basic_salary="{{ number_format(@$board->applicant->job->plantilla_item->basic_salary,2) }}">{!! @$board->applicant->getFullName() !!}</option>
        @endforeach
    </select>

    @endif
    </div>

    <div class="col-7 text-center">
        {{ Form::label('','CERTIFICATION',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('position', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('position', @$casual->applicant->job->plantilla_item->position->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'position'
            ])
        }}
    </div>

    {{ Form::label('', 'Highest Ranking HRMO', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('hrmo',  @$casual->hrmo, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>
</div>

<div class="form-group row {{ $errors->has('employee_status') ? 'has-error' : ''}}">
    {{ Form::label('status', 'Employee Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('employee_status', config('params.employee_status'),  @$casual->employee_status,[
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select employee status',
                'required' => 'true'
            ])
        }}

        {!! $errors->first('employee_status', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Date Sign</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="hrmo_date_sign"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{  @$casual->hrmo_date_sign}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

</div>

<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right">Period of Emp. From</label>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_emp_from"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{  @$casual->period_emp_from}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
    

    <div class="col-7 text-center">
        {{ Form::label('','APPOINTING AUTHORITY',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right">Period of Emp. To</label>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_emp_to"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{  @$casual->period_emp_to}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    {{ Form::label('','Appointing Officer', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('appointing_officer',  @$casual->appointing_officer,['class' => 'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('nature_of_appointment', config('params.nature_of_appointment'),  @$casual->nature_of_appointment,[
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select nature of appointment'
            ])
        }}
    </div>

     <label class="col-12 col-sm-3 col-form-label text-sm-right">Date Sign</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_sign"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$casual->date_sign}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    
</div>

<div class="form-group row">
    {{ Form::label('office', 'Office', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('office', @$casual->applicant->job->plantilla_item->office->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'office'
            ])
        }}
    </div>

   

</div>

<div class="form-group row">
    {{ Form::label('item_no', 'Item No.', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('item_no', @$casual->applicant->job->plantilla_item->item_number, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'item_number'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Monthly Rate', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('monthlyrate', number_format(@$casual->applicant->job->plantilla_item->basic_salary,2), [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'placeholder' => 'PHP 0',
                'id' => 'basic_salary'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Daily Wage', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('daily_wage', number_format(@$casual->applicant->job->plantilla_item->basic_salary / 22,2), [
                'class' => 'form-control form-control-sm',
                'readonly' => true,
                'id' => 'daily_wage'
            ])
        }}
    </div>
</div>



<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="id" value="{{ @$casual->id}}">
    
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        $('#applicant_id').change(function(){
            position    = $(this).find(':selected').data('position');
            office      = $(this).find(':selected').data('office');
            itemNumber  = $(this).find(':selected').data('item_number');
            salaryGrade = $(this).find(':selected').data('salary_grade');
            basicSalary = $(this).find(':selected').data('basic_salary');
            dailyWage = Number(basicSalary) / 22;

            $("#position").val(position);
            $("#office").val(office);
            $("#item_number").val(itemNumber);
            $("#salary_grade").val(salaryGrade);
            $("#basic_salary").val(basicSalary);
            $("#daily_wage").val(dailyWage.toFixed(2));

        });
      });
    </script>
@endsection
