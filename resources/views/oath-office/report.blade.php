@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 10pt;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-4">
    <div class="col-sm-3 font-italic">CS Form No. 32 <br> Revised 2018</div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-12 text-center">
      <h4 class="p-0 m-0 font-weight-bold">Republic of the Philippines</h4>
      <div class="font-weight-bold">METROPOLITAN WATERWOKS AND SEWERAGE SYSTEM - REGULATORY OFFICE</div>
      <div>3rd Floor Engineering Bldg., MWSS Complex,</div>
      <div>Katipunan Road, Balara, Quezon City</div>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h3><b>OATH OF OFFICE</b></h3>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-12">
      <p class="text-justify" style="text-indent: 50px;">
        I, <b><u>{!! $oathoffice->applicant->getFullName() !!}</u></b> of <b><u>{!! $oathoffice->applicant->permanent_house_number !!} {!! $oathoffice->applicant->permanent_street !!} {!! $oathoffice->applicant->permanent_barangay !!} {!! $oathoffice->applicant->city !!}</u></b> having been appointed to the position of <b><u>{!! $oathoffice->applicant->job->plantilla_item->position->Name !!}</u></b> hereby solemnly swear, that I will faithfully discharge to the best of my ability, the duties of my present position and of all others that I may hereafter hold under the Republic of the Philippines; that I will bear true faith and allegiance to the same; that I will obey the laws, legal orders, and decrees promulgated by the duly constituted authorities of the Republic of the Philippines; and that I impose this obligation upon myself voluntarily, without mental reservation or purpose of evasion.
      </p>
      <p style="text-indent: 50px;">SO HELP ME GOD.</p>
    </div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-6"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center"><b>{!! $oathoffice->applicant->getFullName() !!}</b></div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-6"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center border-top border-dark">(Signature over Printed Name of the Appointee)</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-2 text-right">Government ID: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-2 text-right">ID Number: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-2 text-right">Date Issued: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <hr>

  <div class="row mb-8">
    <div class="col-sm-12">
      <p style="text-indent: 50px;">Subscribed and sworn to before me this _______ day of ___________________, 20___ in __________________________________, Philippines.</p>
    </div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-6"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center">
</div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-6"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center border-top border-dark">(Signature over Printed Name of the Appointing Officer/Authority/ Head of Office)</div>
  </div>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection