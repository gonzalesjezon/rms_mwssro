@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Add Oath of Office</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an oath of office in the form below.</span></div>
                <div class="card-body">
                    @include('oath-office._form', [
                        'action' => $action,
                        'method' => 'POST',
                        'boardings' => $boardings,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
