@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 10 <br> Series of 2017</div>
  </div>

  <div class="row mb-6">
    <div class="col-12 text-center">
      <h4 class="p-0 m-0 font-weight-bold">Republic of the Philippines</h4>
      <div class="font-weight-bold">METROPOLITAN WATERWOKS AND SEWERAGE SYSTEM - REGULATORY OFFICE</div>
      <div>3rd Floor Engineering Bldg., MWSS Complex,</div>
      <div>Katipunan Road, Balara, Quezon City</div>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h3><b>ACCEPTANCE OF RESIGNATION</b></h3>
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
      Date:  {!! date('F, d Y',time()) !!}
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-sm-12">
      <u>{!! $resignation->applicant->getFullName() !!}</u> <br>
      <u>{!! $resignation->applicant->barangay !!} {!! $resignation->applicant->street !!} {!! $resignation->applicant->city !!}</u>
    </div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-12">
      <p>Sir/Madam:</p>
      <p style="text-indent: 50px;" class="mb-6">In reply to your letter dated <u>{!! date('F, d Y',strtotime($resignation->letter_date)) !!} </u>tendering your resignation from the position of <u>{!! $resignation->applicant->job->psipop->position_title !!}</u> in <u>{!! $resignation->applicant->job->psipop->division->name !!}</u>, may I inform you that the same is hereby accepted to take effect on <u>{!! date('F, d Y',strtotime($resignation->resignation_date)) !!}</u>.</p>
      <p style="text-indent: 50px;">Your services while employed from this Office have been rated as ________________ , for your reference.</p>
    </div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-7"></div>
    <div class="col-sm-5">Very truly yours,</div>
  </div>

  <div class="row mb-0">
    <div class="col-sm-7"></div>
    <div class="col-sm-3 text-center">{!! $resignation->appointing_officer !!}</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-7"></div>
    <div class="col-sm-3 border-top border-dark text-center"> Appointing Officer/Authority</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-3">Received by: </div>
  </div>

  <div class="row mb-0">
    <div class="col-sm-1"></div>
    <div class="col-sm-3 text-center ">{!! $resignation->applicant->getFullName() !!}</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-1"></div>
    <div class="col-sm-3 border-top border-dark text-center">Signature over Printed Name</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-1"></div>
    <div class="col-sm-4">Date: </div>
  </div>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection