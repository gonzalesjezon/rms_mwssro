@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Acceptance of Resignation</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an acceptance of resignation in the form below.</span></div>
                <div class="card-body">
                    @include('acceptance_resignation._form', [
                        'action' => 'AcceptanceResignationController@store',
                        'method' => 'POST',
                        'applicants' => @$applicants
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
