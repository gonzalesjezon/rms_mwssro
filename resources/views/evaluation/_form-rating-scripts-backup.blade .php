<!-- JS Libraries -->
<script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<!-- frontend validation -->
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>

<!-- Modal -->
<script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
        type="text/javascript"></script>
<script>
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#evaluation-form').parsley();

    $.fn.niftyModal('setDefaults', {
      overlaySelector: '.modal-overlay',
      contentSelector: '.modal-content',
      closeSelector: '.modal-close',
      classAddAfterOpen: 'modal-show',
    });

    document.getElementById('print-button').onclick = function() {
      printElement(document.getElementById('printThis'));
    };

    function printElement(elem) {
      const domClone = elem.cloneNode(true);
      const printSection = document.createElement('div');
      printSection.id = 'printSection';
      printSection.appendChild(domClone);
      $('body').append(printSection);
      $('.be-wrapper').hide();
      window.print();
      $('.be-wrapper').show();
      $('#printSection').remove();
    }

    /*
    |-------------------------------------------
    | Computation Section
    |-------------------------------------------
     */
    $('.performance').on('keyup blur', function() {
      computePerformance();
    });

    // computes performance rating
    function computePerformance() {
      let performance = $('#performance').val();
      let performanceDivide = $('#performance_divide').val();
      // let performanceAverage = Number(performance) / Number(performanceDivide);
      let performanceAverage = $('#performance_average').val();
      // $('#performance_average').val(performanceAverage);
      let performancePercent = $('#performance_percent').val();
      let performanceScore = performanceAverage * Number(performancePercent / 100);
      $('#performance_score').val(parseFloat(performanceScore).toFixed(2));
    }

    $('.education_training').on('keyup blur', function() {
      computeEducationTraining();
    });

    function computeEducationTraining() {
      let minimumEducationPoints = $('#minimum_education_points').val();
      let minimumTrainingPoints = $('#minimum_training_points').val();
      let educationPoints = $('#education_points').val();
      let trainingPoints = $('#training_points').val();
      // let educationTrainingSum = Number(minimumEducationPoints) + Number(minimumTrainingPoints) +
      //   Number(educationPoints) + Number(trainingPoints);
      let educationTrainingSum = $('#education_training_total_points').val();
      let educationTrainingPercent = $('#education_training_percent').val();

      let educationTrainingScore = educationTrainingSum * Number(educationTrainingPercent / 100);
      $('#education_training_score').val(parseFloat(educationTrainingScore).toFixed(2));
    }

    $('.experience_accomplishments').on('keyup blur', function() {
      computeExperienceAccomplishments();
    });

    function computeExperienceAccomplishments() {
      let minimumExperienceRequirement = $('#minimum_experience_requirement').val();
      let additionalPoints = $('#additional_points').val();
      // let experienceAccomplishmentsTotalPoints = Number(minimumExperienceRequirement) + Number(additionalPoints);
      // $('#experience_accomplishments_total_points').val(experienceAccomplishmentsTotalPoints);
      let experienceAccomplishmentsTotalPoints = $('#experience_accomplishments_total_points').val();
      let experienceAccomplishmentsPercent = $('#experience_accomplishments_percent').val();
      let experienceAccomplishmentsScore = experienceAccomplishmentsTotalPoints *
        Number(experienceAccomplishmentsPercent / 100);
      $('#experience_accomplishments_score').val(parseFloat(experienceAccomplishmentsScore).toFixed(2));
    }

    $('.potential').on('keyup blur', function() {
      computePotential();
    });

    function computePotential() {
      let potentialPercentageRating = $('#potential_percentage_rating').val();
      let potentialPercent = $('#potential_percent').val();
      let potentialScore = potentialPercentageRating * Number(potentialPercent / 100);
      $('#potential_score').val(parseFloat(potentialScore).toFixed(2));
    }

    $('.psychosocial').on('keyup blur', function() {
      computePsychosocial();
    });

    function computePsychosocial() {
      let psychosocialPercentageRating = $('#psychosocial_percentage_rating').val();
      let psychosocialPercent = $('#psychosocial_percent').val();
      let psychosocialScore = psychosocialPercentageRating * Number(psychosocialPercent / 100);
      $('#psychosocial_score').val(parseFloat(psychosocialScore).toFixed(2));
    }

    $('.examination').on('keyup blur', function() {
      computeExamination();
    });

    function computeExamination() {
      let examinationPercentageRating = $('#examination_percentage_rating').val();
      let examinationPercent = $('#examination_percent').val();
      let examinationScore = examinationPercentageRating * Number(examinationPercent / 100);
      $('#examination_score').val(parseFloat(examinationScore).toFixed(2));
    }
  });

  $('.performance, .education_training, .experience_accomplishments, .potential, .psychosocial .examination').on('keyup blur', function() {
    computeTotalPercentage();
    computeTotalScores();
  });

  function computeTotalPercentage() {
    // compute total percentage
    let performancePercent = $('#performance_percent').val();
    let educationTrainingPercent = $('#education_training_percent').val();
    let experienceAccomplishmentsPercent = $('#experience_accomplishments_percent').val();
    let potentialPercent = $('#potential_percent').val();
    let psychosocialPercent = $('#psychosocial_percent').val();
    let examinationPercent = $('#examination_percent').val();

    let totalPercentage = Number(performancePercent)
      + Number(educationTrainingPercent)
      + Number(experienceAccomplishmentsPercent)
      + Number(potentialPercent)
      + Number(psychosocialPercent);
      + Number(examinationPercent);

    $('#total_percent').val(totalPercentage);
  }

  function computeTotalScores() {

    // compute total score
    let performanceScore = $('#performance_score').val();
    let educationTrainingScore = $('#education_training_score').val();
    let experienceAccomplishmentsScore = $('#experience_accomplishments_score').val();
    let potentialScore = $('#potential_score').val();
    let psychosocialScore = $('#psychosocial_score').val();
    let examinationScore = $('#examination_score').val();

    let totalScore = Number(performanceScore)
      + Number(educationTrainingScore)
      + Number(experienceAccomplishmentsScore)
      + Number(potentialScore)
      + Number(psychosocialScore);
      + Number(examinationScore);

    console.log('performanceScore' + '--' + performanceScore);
    console.log('educationTrainingScore' + '--' + educationTrainingScore);
    console.log('experienceAccomplishmentsScore' + '--' + experienceAccomplishmentsScore);
    console.log('potentialScore' + '--' + potentialScore);
    console.log('psychosocialScore' + '--' + psychosocialScore);
    console.log('examinationScore' + '--' + examinationScore);

    $('#total_score').val(parseFloat(totalScore).toFixed(2));
  }
</script>