@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    .bg-secondary{
      background-color: #878787 !important;
  }

}
</style>
@endsection

@section('content')
<div style="width: 960px;margin: auto;">
<div class="row mb-4">
  <div class="col-sm-3"><img src="{{URL::asset('img/pcc-logo-small.png')}}" class="img-fluid" alt="pcc logo"/></div>
</div>

<div class="row mb-4 text-center">
  <div class="col-sm-12"><h3><b>INDIVIDUAL ASSESSMENT FORM</b></h3></div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Name</b></div>
  <div class="col-3 ">: {{$applicant->getFullname()}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Present Position</b></div>
  <div class="col-3 ">: {!! $applicant->job->plantilla_item->position->Name !!}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Division/Office</b></div>
  <div class="col-5">: {{$applicant->job->plantilla_item->division->Name}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Vacant Position/JG/Div./Office</b></div>
  <div class="col-3">: {{$applicant->job->plantilla_item->position->Name}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>CSC Qualification Standards</b></div>
  <div class="col-3">:</div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Education</div>
  <div class="col-8">
    @if($applicant->education)
        <?php
            $educ_3 = false;
            $educ_4 = false;
            $educ_5 = false;
         ?>
        @foreach($applicant->education as $education)

        @if($education->educ_level == 1)
        <p class="font-weight-bold p-0 m-0 ml-2">Primary School</p>
        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
        @endif

        @if($education->educ_level == 2)
        <p class="font-weight-bold p-0 m-0 ml-2">Secondary School</p>
        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
        @endif

        @if($education->educ_level == 3)
            @if($educ_3 == false)
            <p class="font-weight-bold p-0 m-0 ml-2">Vocational School</p>
            <?php $educ_3 = true; ?>
            @endif
        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
        @endif

        @if($education->educ_level == 4)
            @if($educ_4 == false)
            <p class="font-weight-bold p-0 m-0 ml-2">College School</p>
            <?php $educ_4 = true; ?>
            @endif
        <p class="p-0 m-0 ml-4">{!! $education->school_name !!} {!! $education->course !!}</p>
        @endif

        @if($education->educ_level == 5)
            @if($educ_5 == false)
                <p class="font-weight-bold p-0 m-0 ml-2">Graduate School</p>
                <?php $educ_5 = true; ?>
            @endif
        <p class="p-0 m-0 ml-4">{!! $education->school_name !!} {!! $education->course !!}</p>
        @endif

        @endforeach
    @endif
  </div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Experience</div>
  <div class="col-8">
    @if($applicant->workexperience)
        @foreach($applicant->workexperience as $experience)
        <p class="p-0 m-0 ml-2">{!! $experience->position_title !!}</p>
        @endforeach
    @endif
  </div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Eligibility</div>
  <div class="col-8">
    @if($applicant->training)
        @foreach($applicant->training as $training)
        <p class="p-0 m-0 ml-2">{!! $training->title_learning_programs !!}</p>
        @endforeach
    @endif
  </div>
</div>

<div class="row mb-4 text-left">
  <div class="offset-1 col-1">Training</div>
  <div class="col-8">
    @if($applicant->eligibility)
        @foreach($applicant->eligibility as $eligibility)
        <p class="p-0 m-0 ml-2">{!! $eligibility->name !!}</p>
        @endforeach
    @endif
  </div>
</div>

<div class="row mb-4 text-left">
  <div class="col-2"><b>Evaluation Made as of:</b></div>
  <div class="col-3">: {{@$applicant->evaluation->created_at}}</div>
</div>

<div class="row">
  <div class="col text-light bg-secondary">I. PERFORMANCE (40%)</div>
</div>

<div class="form-group row">
  <div class="col">For Transferees:</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->performance }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Rating 1 + Rating 2</div>
  </div>

  <label for=""><h4 class="mt-1">/</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_divide }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_average }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="offset-1"><h4 class="mt-1">X</h4></div>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<div class="row">
  <div class="col">
    <label for="">* For Non-Transferee, a grade of Satisfactory rating is given.</label>
  </div>
</div>

<!-- Education & Training -->
<div class="row">
  <div class="col text-light bg-secondary">II. EDUCATION & TRAINING (20%)</div>
</div>

<div class="row">
  {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->eligibility}}
  </div>
</div>

<div class="row">
  {{ Form::label('training', 'Training', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->training}}
  </div>
</div>

<div class="row">
  {{ Form::label('Seminar', 'Seminar', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->seminar}}
  </div>
</div>

<div class="row">
  {{ Form::label('minimum_education_points', 'Minimum Educational Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-1">
    <p>{{ @$applicant->evaluation->minimum_education_points}} PTS</p>
  </div>
</div>

<div class=" row">
  {{ Form::label('minimum_training_points', 'Minimum Training Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-1">
   <p>{{ @$applicant->evaluation->minimum_training_points}} PTS</p>
  </div>
</div>

<div class="row">
  {{ Form::label('ratings_excess', 'Ratings in Excess of the Minimum:', ['class'=>'col-4 mt-1 font-weight-bold']) }}
</div>

<div class="row">
  {{ Form::label('education_points', 'Education', ['class'=>'col-2 mt-2']) }}
  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->education_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Points Rating</div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('training_points', 'Training', ['class'=>'col-2 mt-2']) }}
  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->training_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Points Rating</div>
  </div>

  <label for="" class="offset-1 mt-3">=</label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->education_training_total_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Total Points</div>
  </div>

  <div class="mt-3">X</div>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->education_training_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Points Weight</div>
  </div>

  <label for="" class="mt-3">=</label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->education_training_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Experience & Outstanding Accomplishments -->
<div class="form-group row">
  <div class="col text-light bg-secondary">III. EXPERIENCE & OUTSTANDING ACCOMPLISHMENTS (20%)</div>
</div>

<div class="row">
  {{ Form::label('relevant_positions_held', 'Relevant Positions Held', ['class'=>'col-3 mt-1']) }}
  <div class="col-4">
    <p>{{ @$applicant->evaluation->relevant_positions_held}}</p>
  </div>
</div>

<div class="row">
  {{ Form::label('minimum_experience_requirement', 'Minimum Experience Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-4">
    <p>{{ @$applicant->evaluation->minimum_experience_requirement}} PTS</p>
  </div>
</div>

<div class="row">
  {{ Form::label('additional_points', 'Additional Points (in excess of the minimum requirement)', ['class'=>'col-3 mt-1']) }}
  <div class="col-3">
    <p>{{ @$applicant->evaluation->additional_points}} PTS</p>
  </div>
</div>

<div class="row">
  <label for="" class="offset-3 mt-3">=</label>
  <div class="col-3 text-center">
    {{ @$applicant->evaluation->experience_accomplishments_total_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Total Points</div>
  </div>

  <label for="" class="mt-3">X</label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->experience_accomplishments_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for="" class="mt-3">=</label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->experience_accomplishments_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Potential -->
<div class="row">
  <div class="col text-light bg-secondary">IV. POTENTIAL (10%)</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->potential}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">R1 + R2 + R3 + R4 + R5</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->potential_average_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->potential_percentage_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Percentage Rating</div>
  </div>

  <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->potential_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->potential_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Potential -->
<div class="form-group row">
  <div class="col text-light bg-secondary">V. PSYCHOSOCIAL ATTRIBUTES & PERSONALITY TRAITS (10%)</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">R1 + R2 + R3 + R4 + R5</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_average_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_percentage_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Percentage Rating</div>
  </div>

  <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Examination -->
<div class="form-group row">
  <div class="col text-light bg-secondary">VI. EXAMINATION (5%)</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->examination}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">R1 + R2 + R3 + R4 + R5</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->examination_average_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->examination_percentage_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Percentage Rating</div>
  </div>

  <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->examination_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->examination_score}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<hr>

<div class="form-group row">
  <div class="col-8 text-light bg-secondary">TOTAL</div>
  <div class="col text-light bg-secondary">{{ @$applicant->evaluation->total_percent}}</div>
  <div class="col text-light bg-secondary">{{ @$applicant->evaluation->total_score}}</div>
</div>

<div class="form-group row text-center">
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
</div>

<div class="form-group row text-center">
  <div class="col">
    {{ @$evaluation->evaluated_by}}
    <hr>
  </div>
  <div class="col">
    {{ @$evaluation->reviewed_by}}
    <hr>
  </div>
  <div class="col">
    {{  @$evaluation->noted_by}}
    <hr>
  </div>
</div>
</div>


  <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection