<!-- JS Libraries -->
<script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<!-- frontend validation -->
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>

<!-- Modal -->
<script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
        type="text/javascript"></script>
<script>
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#evaluation-form').parsley();

    $.fn.niftyModal('setDefaults', {
      overlaySelector: '.modal-overlay',
      contentSelector: '.modal-content',
      closeSelector: '.modal-close',
      classAddAfterOpen: 'modal-show',
    });

    document.getElementById('print-button').onclick = function() {
      printElement(document.getElementById('printThis'));
    };

    function printElement(elem) {
      const domClone = elem.cloneNode(true);
      const printSection = document.createElement('div');
      printSection.id = 'printSection';
      printSection.appendChild(domClone);
      $('body').append(printSection);
      $('.be-wrapper').hide();
      window.print();
      $('.be-wrapper').show();
      $('#printSection').remove();
    }

    /*
    |-------------------------------------------
    | Computation Section
    |-------------------------------------------
     */
    $('.performance').on('keyup blur', function() {
      computePerformance();
    });

    // computes performance rating
    function computePerformance() {
      let performance = $('#performance_total').val();
      // let performanceAverage = Number(performance) / Number(performanceDivide);
      let performanceAverage = $('#performance_average').val();
      // $('#performance_average').val(performanceAverage);
      let performancePercent = $('#performance_percent').val();
      let performanceScore = performanceAverage * Number(performancePercent / 100);
      $('#performance_score').val(parseFloat(performanceScore).toFixed(2));
    }

    $('.education_training').on('keyup blur', function() {
      computeEducationTraining();
    });

    function computeEducationTraining() {
      let educationPoints = $('#education_points').val();
      let graduatePoints = $('#graduate_points').val();
      let educationGraduateSum = Number(educationPoints) + Number(graduatePoints);

      $('#education_graduate_total_points').val(educationGraduateSum)
      $('#educ_total').val(educationGraduateSum)

      let educationGraduatePercent = $('#education_graduate_percent').val();
      let educationGraduateScore = educationGraduateSum * Number(educationGraduatePercent / 100);

      $('#education_graduate_score').val(parseFloat(educationGraduateScore).toFixed(2));
    }

    $('.experience').on('keyup blur', function() {
      computeExperienceAccomplishments();
    });

    function computeExperienceAccomplishments() {
      let experiencePoints = $('#experience_points').val();
      let numberYearPoints = $('#number_of_year_points').val();

      let experienceTotalPoints = Number(experiencePoints) + Number(numberYearPoints);
      $('#experience_total_points').val(experienceTotalPoints);
      $('#experience_total').val(experienceTotalPoints);

      let experiencePercent = $('#experience_percent').val();
      let experienceScore = experienceTotalPoints * Number(experiencePercent / 100);
      $('#experience_score').val(parseFloat(experienceScore).toFixed(2));
    }

    $('.potential').on('keyup blur', function() {
      computePotential();
    });

    function computePotential() {
      let r1Point = $('#potential_r1').val();
      let r2Point = $('#potential_r2').val();
      let r3Point = $('#potential_r3').val();
      let r4Point = $('#potential_r4').val();
      let potentialTotalPoint = Number(r1Point) + Number(r2Point) + Number(r3Point) + Number(r4Point);

      $('#potential_total_points').val(potentialTotalPoint);
      $('#potential_total').val(potentialTotalPoint);

      let potentialPercent = $('#potential_percent').val();
      let potentialScore = potentialTotalPoint * Number(potentialPercent / 100);
      $('#potential_score').val(parseFloat(potentialScore).toFixed(2));
    }

    $('.psychosocial').on('keyup blur', function() {
      computePsychosocial();
    });

    function computePsychosocial() {
      let r1Psychosocial = $('#psychosocial_r1').val();
      let r2Psychosocial = $('#psychosocial_r2').val();
      let r3Psychosocial = $('#psychosocial_r3').val();
      let r4Psychosocial = $('#psychosocial_r4').val();
      let psychosocialTotalPoint = Number(r1Psychosocial) + Number(r2Psychosocial) + Number(r3Psychosocial) + Number(r4Psychosocial);

      $('#psychosocial_total_points').val(psychosocialTotalPoint);
      $('#psychosocial_total').val(psychosocialTotalPoint);

      let psychosocialPercent = $('#psychosocial_percent').val();
      let psychosocialScore = psychosocialTotalPoint * Number(psychosocialPercent / 100);
      $('#psychosocial_score').val(parseFloat(psychosocialScore).toFixed(2));
    }

    $('.examination').on('keyup blur', function() {
      computeExamination();
    });

    function computeExamination() {
      let examinationTotalPoints= $('#examination_total_points').val();
      let examinationPercent = $('#examination_percent').val();
      let examinationScore = examinationTotalPoints * Number(examinationPercent / 100);
      $('#examination_score').val(parseFloat(examinationScore).toFixed(2));
    }

    $('.performance, .education_training, .experience, .potential, .psychosocial, .examination').trigger('keyup');
  });

  $('.performance, .education_training, .experience, .potential, .psychosocial .examination').on('keyup blur', function() {
    computeTotalPercentage();
    computeTotalScores();
  });

  function computeTotalPercentage() {
    // compute total percentage
    let performancePercent = $('#performance_percent').val();
    let educationGraduatePercent = $('#education_graduate_percent').val();
    let experiencePercent = $('#experience_percent').val();
    let potentialPercent = $('#potential_percent').val();
    let psychosocialPercent = $('#psychosocial_percent').val();
    let examinationPercent = $('#examination_percent').val();

    let totalPercentage = Number(performancePercent)
      + Number(educationGraduatePercent)
      + Number(experiencePercent)
      + Number(potentialPercent)
      + Number(psychosocialPercent);
      + Number(examinationPercent);

    $('#total_percent').val(totalPercentage);
  }

  function computeTotalScores() {

    // compute total score
    let performanceScore = $('#performance_score').val();
    let educationGraduateScore = $('#education_graduate_score').val();
    let experienceScore = $('#experience_score').val();
    let potentialScore = $('#potential_score').val();
    let psychosocialScore = $('#psychosocial_score').val();
    let examinationScore = $('#examination_score').val();
    let totalScore = Number(performanceScore)
      + Number(educationGraduateScore)
      + Number(experienceScore)
      + Number(potentialScore)
      + Number(psychosocialScore);
      + Number(examinationScore);

    // console.log('performanceScore' + '--' + performanceScore);
    // console.log('educationGraduateScore' + '--' + educationGraduateScore);
    // console.log('experienceScore' + '--' + experienceScore);
    // console.log('potentialScore' + '--' + potentialScore);
    // console.log('psychosocialScore' + '--' + psychosocialScore);
    // console.log('examinationScore' + '--' + examinationScore);

    $('#total_score').val(parseFloat(totalScore).toFixed(2));
  }
</script>