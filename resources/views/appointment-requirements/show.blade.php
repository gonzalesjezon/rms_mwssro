@extends('layouts.app')

@section('content')

    <div class="page-head">
        <h2 class="page-head-title">Appointment - Requirements</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('appointment-requirements.index' ) }}">
                        Applicants ( {{ ($requirement) ? $requirement->applicant->getFullName() : '' }} )</a></li>
            </ol>
        </nav>
    </div>

    <!-- Applicant Form -->
    @if(!$documentView)
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                {{ ($requirement) ? $requirement->applicant->getFullName() : '' }}
                <span class="card-subtitle">
                Applied For: {{ $requirement->applicant->job->psipop->position_title }} / Applied On: {{ $requirement->created_at }}
                </span>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Attached Documents
                    <span class="card-subtitle">All applicant documents attached below</span>
                </div>
                <div class="card-body">
                @if($requirement)

                    @if($requirement->pds_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Personal Data Sheet</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->pds_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->pds_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->saln_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Statement of Assets, Liabilities and Net Worth (SALN)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->saln_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->saln_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->eligibility_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Authenticated Certificate of Civil Service Eligibility or PRC Board Certificate</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->eligibility_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->eligibility_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->training_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Certified True Copies of latest Certificates of Trainging/Seminars</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->training_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->training_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->psa_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Copy of Certificate of Live Birth (PSA)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->psa_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->psa_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->tor_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Certified True Copy of Transcript of Records</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->tor_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->tor_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->diploma_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Certified True Copy of your Diploma</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->diploma_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->diploma_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->philhealth_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Copy of Philhealth MDR</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->philhealth_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->philhealth_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->nbi_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Latest National Bureau of Investigation Clearance</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->nbi_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->nbi_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->medical_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Original Copy of Medical Certificate (Urinalysis, Chest X-ray,CBC, Drug test)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->medical_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->medical_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->bir_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">BIR Form 2316 for those with previous employer</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->bir_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->bir_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    @if($requirement->coe_path)
                        <div class="row">
                            <div class="col-5 mt-1"><span class="badge badge-success">Clearance from previous employer</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->coe_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->coe_path) }}" download >
                                    <span class="badge badge-info">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
        });
    </script>
@endsection
