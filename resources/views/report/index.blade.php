@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Reports</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
          </div>
          <div class="card-body p-2">
            <div class="form-group row">
              {{ Form::label('reports_name', 'Reports Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('reports_name', $reports, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select report',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row d-none hide" id="select_position">
              {{ Form::label('select_job', 'Position', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="select_job" id="select_job">
                  <option value="0">Select position</option>
                  @foreach($jobs as $job)
                  <option value="{{$job->id}}">{{$job->plantilla_item->position->Name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row d-none hide" id="select_applicant">
              {{ Form::label('applicant_name', 'Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('name', $applicants, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select applicant',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'HRMO', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="hrmo">
                  @foreach($employeeinfo as  $info)
                    @if($info->employee)
                      <option value="{{ $info->RefId }}" data-sign="{{ $info->employee->getFullName().'|'.@$info->position->Name  }}">{!! $info->employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'Agency Head', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="agency_head">
                  @foreach($employeeinfo as  $info)
                    @if($info->employee)
                      <option value="{{ $info->RefId }}" data-sign="{{ $info->employee->getFullName().'|'.@$info->position->Name  }}">{!! $info->employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'CSC Officical', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="csc_official">
                  @foreach($employeeinfo as  $info)
                    @if($info->employee)
                      <option value="{{ $info->RefId }} " data-sign="{{ $info->employee->getFullName().'|'.@$info->position->Name  }}">{!! $info->employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>

            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-2 col-form-label text-sm-right">Print Date </label>
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                  <input size="16" type="text" value="" name="print_date"
                         class="form-control form-control-sm" id="print_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

<!--             <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Sex </label>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-female"></span></div><span class="icon-class"> </span>
                </div>
              </div>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-male"></span></div><span class="icon-class"> </span>
                </div>
              </div>
            </div> -->

            <hr>

            <div class="form-group row">
              <div class="col-4 offset-6">
                <buton class="btn btn-secondary" id="preview">Preview</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      var reportName;
      var bool = false;
      $(document).on('change','#reports_name',function(){
          bool = false;
          jobId  = '';
          printDate = '';
          $('.unselect').val([]);

          reportName = $(this).find(':selected').val();
          $('.hide-all').addClass('d-none');

          switch(reportName){
            case 'selection_lineup':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'checklist':
              $('#select_applicant').removeClass('d-none');
              $('#select_position').addClass('d-none');
              bool = true;
              break;
            case 'resignation_acceptance':
              $('#select_applicant').removeClass('d-none');
              $('#select_position').addClass('d-none');
              bool = true;
              break;
            case 'preliminary_evaluation':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'talent_placement_list':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'summary_of_comparative':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'oath_office':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'appointment_form_regulated':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'medical_certificate':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'erasures_alteration':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;

            case 'appointments_issued':
              $('.cdm').removeClass('d-none');
              bool = true;
              break;
            default:
              $('.hide').addClass('d-none')
              bool = false;
              break;
          }
      });

      var jobId;
      $(document).on('change','#select_job',function(){
          jobId = $(this).find(':selected').val();
          bool = true;

      })

      var appId;
      $(document).on('change','#select_applicant',function(){
          appId = $(this).find(':selected').val();
          bool = true;

      })

      var printDate;
      $(document).on('change','#print_date',function(){
        printDate = $(this).val();
      });

      $(document).on('click','#preview',function(){

          var param = "";
          var sign  = "";

          let hrmo        = $('#hrmo :selected').data('sign');
          let agencyHead  = $('#agency_head :selected').data('sign');
          let cscOfficial = $('#csc_official :selected').data('sign');

          if(jobId){
            id = jobId;
          }else{
            id = appId;
          }

          arrSign = [];
          if(reportName == 'appointments_issued')
          {
            arrSign = {
              'hrmo':hrmo, 
              'agency_head':agencyHead,
              'csc_official':cscOfficial,
            };
          }

          param = (id) ? 'id='+id : '';
          date = (printDate) ? 'date='+printDate : '';
          sign = (!Array.isArray(arrSign) || !arrSign.length) ? 'sign='+JSON.stringify(arrSign)+'&' : '';

          var href = '';
          if(bool == true){
            if(param || date || sign){
              href = window.location+'/'+reportName+'?'+param+date+sign;
            }else{
              alert('Select position or applicant first!');
              return false;
            }
          }else{

              href = window.location+'/'+reportName;

          }

          window.open(href, '_blank');
      });

    });
  </script>
@endsection
