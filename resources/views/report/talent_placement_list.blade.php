@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-8">
    <div class="col-sm-3 text-left">
    	<div class="border p-1">
	    	<p class="p-0 m-0 " style="line-height: 15px;">FORM-RO-FM-HR-15</p>
	    	<p class="p-0 m-0 " style="line-height: 15px;">Rev 0</p>
	    	<p class="p-0 m-0 " style="line-height: 15px;">Effectivity Date: June 04, 2018</p>
    	</div>
    </div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12 text-center">
  		<div style="font-size: 16px;" class="font-weight-bold">TALENT PLACEMENT LIST</div>
  		<div>The Candidate/s Screened, Evaluated and Ranked</div>
      <div>By the Human Resource Merit Promotion Selection Board for Second Level in a meeting <br> Held on </div>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-2">Position to be filled up:</div>
  	<div class="col-4">{!! $job->plantilla_item->position->Name !!}</div>
  </div>

  <hr>

  <div class="row mb-2">
  	<div class="col-1"></div>
  	<div class=" col-3 text-center font-weight-bold">CANDIDATE/S</div>
  	<div class=" col-4 text-center font-weight-bold">RANKING</div>
  	<div class=" col-4 text-center font-weight-bold">CHOICE/APPROVAL OF <br> <u>THE  CHIEF REGULATOR</u></div>
  </div>

  <?php
  	$max_points = 0;
  ?>

  @foreach($evaluations as $key => $evaluation)
  <div class="row mb-1">
  	<div class="col-1">{{$key+1}} .</div>
  	<div class="col-3">{{ $evaluation->applicant->getFullName() }} <br> ({{$evaluation->total_score}} pts) </div>
  	<?php $max_points += $evaluation->total_score; ?>
  	<div class="col-4 text-center">{{$key+1}}</div>
  </div>
  @endforeach

  <div class="mb-7"></div>

  <div class="row mb-0">
  	<div class="col-5 text-center">*Maximum points {{ $max_points }}</div>
  	<div class="col-3"></div>
  	<div class="col-3 border border-bottom-0 border-right-0 border-left-0 border-dark"></div>
  </div>

  <div class="row mb-4">
  	<div class="col-5 text-center"></div>
  	<div class="col-3"></div>
  	<div class="col-3 text-center">Name of Selected Candidate for Appointment</div>
  </div>

  <hr>

  <div class="row mb-6">
  	<div class="col-12">
  		For The Human Resource Merit Promotion Selection Board
  	</div>
  </div>

  <div class="row mb-8">
  	<div class="col-12 text-center"> <b>Virginia V. Octa</b> <br>Chairperson</div>
  </div>

  <div class="row mb-2">
  	<div class="col-4 text-center"> <b>Crescenciano B. Minas, Jr.</b> <br>End-user, Legal Dept.</div>
  	<div class="col-4 text-center"> <b>Ranjev M. Garcia</b> <br>Member</div>
  	<div class="col-4 text-center"> <b>Alan D. Chuegan</b> <br>Member RO-TUBIG Representative</div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection