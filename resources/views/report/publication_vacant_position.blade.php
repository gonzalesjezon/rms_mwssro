@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th{
      padding: 3px !important;
    }

    .table>thead>tr>th,
    .table>tbody>tr>td{
      border: 1px solid #333 !important;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
  }

  .table>thead>tr>th,
  .table>tbody>tr>td{
    border: 1px solid #333 !important;
  }
</style>
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 9 <br> Revised 2018</div>
    <div class="col-sm-6"></div>
    <div class="col-sm-3 text-center">
    	<p class="border p-1">
    		Electronic copy to be submitted to the CSC FO must be in MS Excel format
    	</p>
    </div>
  </div>

  <div class="row mb-6">
  	<div class="col-sm-12 text-center">
  		<div style="font-size: 16px;" class="font-weight-bold">Republic of the Philippines</div>
  		<div>METROPOLITAN WATERWORKS AND SEWARAGE SYSTEM - REGULATORY OFFICE</div>
      <div class="mb-4">3rd Floor Engineering Bldg., MWSS Complex, <br> Katipunan Road, Balara, Quezon City</div>
  		<div class="font-weight-bold pt-1" style="font-size:16px;">Request for Publication of Vacant Positions</div>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<h4 class="font-weight-bold">To: CIVIL SERVICE COMMISSION (CSC)</h4>
  		<p style="text-indent: 70px;">This is to request the publication of the following vacant positions of <b>METROPOLITAN WATERWORKS AND SEWARAGE SYSTEM - REGULATORY OFFICE</b> in the CSC website:</p>
  	</div>
  </div>

  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4 text-center">
    </div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4 text-center border-top">
  		HRMO
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4">
  		Date
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<table class="table table-fw-widget table-bordered">
  			<thead>
  				<tr class="text-center">
  					<th rowspan="2" style="vertical-align: middle;">No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Position Title</th>
  					<th rowspan="2" style="vertical-align: middle;">Plantilla Item No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Annual Salary</th>
  					<th colspan="5">Qualification Standards</th>
  					<th rowspan="2" style="vertical-align: middle;">Place of Assignment</th>
  				</tr>
  				<tr class="text-center">
  					<th style="vertical-align: middle;">Education</th>
  					<th style="vertical-align: middle;">Training</th>
  					<th style="vertical-align: middle;">Experience</th>
  					<th style="vertical-align: middle;">Eligibility</th>
  					<th style="vertical-align: middle;">Competency <br> (if applicable)</th>
  				</tr>
  			</thead>
        <tbody>
          @foreach($jobs as $key => $job)
          <tr>
            <td class="text-center" style="vertical-align: top">{!! $key+1 !!}</td>
            <td style="vertical-align: top">{{ $job->plantilla_item->position->Name}}</td>
            <td style="vertical-align: top">{{ $job->plantilla_item->item_number}}</td>
            <td style="vertical-align: top">{{ number_format($job->plantilla_item->basic_salary * 12,2)}}</td>
            <td style="vertical-align: top">
              {!! $job->education !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->training !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->experience !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->eligibility !!}
            </td>
            <td style="vertical-align: top" class="text-center"></td>
            <td style="vertical-align: top;" class="text-center">{!! $job->place_of_assignment !!}</td>
          </tr>
          @endforeach
        </tbody>
  		</table>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p>Interested and qualified applicants should signify their interest in writing. Attach the following documents to the application letter and send to the address below not later _____</p>
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<ol>
  			<li>Fully accomplished Personal Data Sheet (PDS) with recent passport-sized picture (CS Form No. 212, Revised 2017) which can be downloaded at www.csc.gov.ph;</li>
  			<li>Performance rating  in the <b>last rating period</b> (if applicable);</li>
  			<li>Photocopy of certificate of eligibility/rating/license; and</li>
  			<li>Photocopy of Transcript of Records.</li>
  		</ol>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>QUALIFIED APPLICANTS</b> are advised to hand in or send through courier/email their application to:</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-4 text-center">
  		<p class="border-bottom border-dark"></p>
  		<p class="border-bottom border-dark">&nbsp;</p>
  		<p class="border-bottom border-dark">3rd Floor Engineering Bldg., MWSS Complex, Katipunan Road, Balara, Quezon City</p>
  		<p class="border-bottom border-dark">(E-mail Address)</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>APPLICATIONS WITH INCOMPLETE DOCUMENTS SHALL NOT BE ENTERTAINED.</b></p>
  	</div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection