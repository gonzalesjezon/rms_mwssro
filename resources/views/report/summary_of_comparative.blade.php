@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

	<div class="row form-group">
		<div class="col-12 text-center">
			<h4 class="font-weight-bold p-0 mb-0">METROPOLITAN WATERWOKS AND SEWERAGE SYSTEM - REGULATORY OFFICE</h4>
			<h5 class="mt-1">3rd Floor Engineering Bldg., MWSS Complex, Katipunan Road, Balara Quezon City</h3>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-12 text-center">
			<h4 class="font-weight-bold">SUMMARY OF COMPARATIVE MATRIX</h4>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-2">
			<label>Position to be filled up.</label>
		</div>
		<div class="col-10">
			{!! $job->plantilla_item->position->Name !!} SG -{!! $job->plantilla_item->salary_grade->Name !!}, ITEM NO. {!! $job->plantilla_item->item_number !!}, {!! $job->plantilla_item->division->Name !!}
		</div>
	</div>

	<table class="table1 table-bordered col-12">
		<thead>
			<tr>
				<th>Criteria</th>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<th class="text-center">{!! $value->applicant->getFullName() !!}</th>
					@endforeach
				@endif
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Performance (23 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->performance_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Experience (22 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->experience_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Education and Training (20 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->education_graduate_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Psychosocial (20 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->psychosocial_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Potential (10 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->potential_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Exam (5 %)</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->examination_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Total Score</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $value->total_score !!}</td>
					@endforeach
				@endif
			</tr>
			<tr>
				<td>Ranking</td>
				@if($assessments)
					@foreach($assessments as $key => $value)
						<td class="text-center">{!! $key+1 !!}</td>
					@endforeach
				@endif
			</tr>
		</tbody>
	</table>

</div>


 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection