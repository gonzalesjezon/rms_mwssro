@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th{
      padding: 3px !important;
      border: 1px solid #000;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
    border: 1px solid #000;
  }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<!-- <div class="dropdown-divider d-print-none"></div> -->

<div class="row">
    <div class="col-12 text-center">
        <h4 class="font-weight-bold pb-0">APPOINTMENT PROCESSING CHECKLISTS</h4>
        <span>(REGULATED/NON-ACCREDITED)</span>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <tr>
                <td class="p-1" style="width: 2in;">Name</td>
                <td class="p-1" colspan="3">{{$appointment->applicant->getFullName()}}</td>
            </tr>
            <tr>
                <td class="p-1" style="width: 2in;">Date of Birth</td>
                <td class="p-1" colspan="3">{!! date('F d,Y',strtotime($appointment->applicant->birthday))!!}</td>

            </tr>
            <tr>
                <td class="p-1" style="width: 2in;">Position Title</td>
                <td class="p-1" style="width: 41% !important;">{{$appointment->applicant->job->plantilla_item->position->Name }}</td>
                <td style="width: 50px ! important;">SG/STEP</td>
                <td>
                    <p class="p-0 m-0" style="text-indent: 10px;">SG {{$appointment->applicant->job->plantilla_item->salary_grade->Name }}</p class="p-0 m-0">
                </td>
            </tr>
            <tr>
                <td class="p-1" style="width: 2in;">Agency</td>
                <td class="p-1" colspan="3">OFFICE OF THE VICE PRESIDENT</td>

            </tr>
            <tr>
                <td class="p-1" style="width: 2in;">Annual Compensation</td>
                <td class="p-1" colspan="3">{{ number_format($appointment->applicant->job->plantilla_item->SalaryAmount * 12,2)  }}</td>

            </tr>
            <tr>
                <td class="p-1" style="width: 2in;">Item Number</td>
                <td class="p-1" colspan="3">{{$appointment->applicant->job->plantilla_item->Name }}</td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <label class="font-weight-bold card-title">Qualification Standards</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1" style="width: 2in;">Criteria</td>
                    <td class="p-1" style="width: 2.8in;">Requirements</td>
                    <td class="p-1" style="width: 2.8in;">Appointee's Qualification <br> (Provide Specific Details)</td>
                    <td class="p-1" colspan="2" >QS MET</td>
                    <td class="p-1">Remarks</td>
                </tr>
                <tr>
                    <td class="p-1">Education</td>
                    <td class="p-1">{!! $appointment->applicant->job->education !!}</td>
                    <td class="p-1">
                        @foreach($appointment->applicant->education as $education)
                        {!! $education->school_name  !!} <br>
                        @endforeach
                    </td>
                    @if(@$appointment->educ_check == 1)
                    <td class="text-center p-1" style="width: 10% !important;" >
                        YES
                    </td>
                    <td class="text-center p-1" style="width: 10% !important;" >
                    @else
                    <td class="text-center p-1" style="width: 10% !important;" >
                    </td>
                    <td class="text-center p-1" style="width: 10% !important;" >
                        NO
                    </td>
                    @endif
                    <td class="p-1 text-center">{{@$appointment->educ_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Experience</td>
                    <td class="p-1">{!! $appointment->applicant->job->experience !!}</td>
                    <td class="p-1">
                        @foreach($appointment->applicant->workexperience as $experience)
                        {!! $experience->position_title  !!} <br>
                        @endforeach
                    </td>
                    @if(@$appointment->exp_check == 1)
                    <td class="text-center p-1" >
                        YES
                    </td>
                    <td></td>
                    @else
                    <td class="text-center p-1" >
                    </td>
                    <td class="text-center p-1" >
                        NO
                    </td>
                    @endif
                    <td class="p-1 text-center">{{@$appointment->exp_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Training</td>
                    <td class="p-1">{!! $appointment->applicant->job->training !!}</td>
                    <td class="p-1">
                        @foreach($appointment->applicant->training as $training)
                        {!! $training->title_learning_programs  !!} <br>
                        @endforeach
                    </td>
                     @if(@$appointment->training_check == 1)
                    <td class="text-center p-1" >
                        YES
                    </td>
                    <td></td>
                    @else
                    <td class="text-center p-1" >
                    </td>
                    <td class="text-center p-1" >
                        NO
                    </td>
                    @endif
                    <td class="p-1 text-center">{{@$appointment->training_remarks}}</td>
                </tr>
                <tr>
                    <td class="p-1">Eligibility</td>
                    <td class="p-1">{!! $appointment->applicant->job->eligibility !!}</td>
                    <td class="p-1">
                        @foreach($appointment->applicant->eligibility as $eligibility)
                        {!! config('params.eligibility_type.'.$eligibility->eligibility_ref) !!} <br>
                        @endforeach
                    </td>
                    @if(@$appointment->eligibility_check == 1)
                    <td class="text-center p-1" >
                        YES
                    </td>
                    <td></td>
                    @else
                    <td class="text-center p-1" >
                    </td>
                    <td class="text-center p-1" >
                        NO
                    </td>
                    @endif
                    <td class="p-1 text-center">{{@$appointment->eligibility_remarks}} </td>
                </tr>
                <tr>
                    <td class="p-1">Others if applicable <br> (e.g., Age, Term of Office)</td>
                    <td class="p-1"></td>
                    <td class="p-1">{{@$appointment->other_qualification}}</td>
                    @if(@$appointment->other_check == 1)
                    <td class="text-center p-1" >
                        YES
                    </td>
                    <td></td>
                    @else
                    <td class="text-center p-1" >
                    </td>
                    <td class="text-center p-1" >
                        NO
                    </td>
                    @endif
                    <td class="p-1 text-center">{{@$appointment->other_remarks}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <label class="font-weight-bold card-title">Common Requirements for Regular Appointments</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1" style="width: 10.2in;">Requirements</td>
                    <td class="p-1">Detail/Compliance</td>
                </tr>
                <tr>
                    <td class="p-1">CS Form 33-A (revised 2017) in triplicate copies</td>
                    <td class="p-1">{{@$appointment->ra_form_33}}</td>
                </tr>
                <tr>
                    <td class="p-1">Employement Status</td>
                    <td class="p-1">{{@$appointment->ra_employee_status}}</td>
                </tr>
                <tr>
                    <td class="p-1">Nature of Appointment</td>
                    <td class="p-1">{{@$appointment->ra_nature_appointment}} </td>
                </tr>
                <tr>
                    <td class="p-1">Appointing Authority</td>
                    <td class="p-1">{{@$appointment->ra_appointing_authority}}</td>
                </tr>
                <tr>
                    <td class="p-1">Date of Signing</td>
                    <td class="p-1">{{@$appointment->ra_date_sign}}</td>
                </tr>
                <tr>
                    <td class="p-1">Date of Pulbication/Posting of Vacant Position</td>
                    <td class="p-1">{{@$appointment->ra_date_publication}}</td>
                </tr>
                <tr>
                    <td class="p-1">Certification by PSB Chairman (at the back of appoitnment) or a copy of the proceedings of PSB's Deliberation</td>
                    <td class="p-1">{{@$appointment->ra_certification}}</td>
                </tr>
                <tr>
                    <td class="p-1">Personal Data Sheet (ra Form 212, Revised 2017) Completely Filled with Picture Attached</td>
                    <td class="p-1">{{@$appointment->ra_pds}}</td>
                </tr>
                <tr>
                    <td class="p-1">Certificate of Eligiblity/License (Authenticated Copy)</td>
                    <td class="p-1">{{@$appointment->ra_eligibility}}</td>
                </tr>
                <tr>
                    <td class="p-1">Position Description Form (PDF)</td>
                    <td class="p-1">{{@$appointment->ra_position_description}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <label class="font-weight-bold card-title">Additional and Specific Cases</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1" style="width: 10.2in;">Requirements</td>
                    <td class="p-1">Detail/Compliance</td>
                </tr>
                <tr>
                    <td class="p-1">
                        Erasures/alterations on the appointment and other supporting documents (Changes duly initialed by authorized officials and accompanies by a communication authenticating changes made)
                    </td>
                    <td class="p-1 text-center">{{@$appointment->ar_01}}</td>
                </tr>
                <tr>
                    <td class="p-1">Appointee with decided administrative/criminal case (certified true copy of decision rendered submitted)</td>
                    <td class="p-1 text-center">{{@$appointment->ar_02}}</td>
                </tr>
                <tr>
                    <td class="p-1">Discrepancy in name/place of birth (Requirements and procedures as amended by CSC Resolution No,991907 dated August 27, 1999)</td>
                    <td class="p-1 text-center">{{@$appointment->ar_03}}</td>
                </tr>
                <tr>
                    <td class="p-1">COMELEC Ban (Exemption from COMELEC)</td>
                    <td class="p-1 text-center">{{@$appointment->ar_04}}</td>
                </tr>
                <tr>
                    <td class="p-1">
                        Non-Disciplinary Demotion
                        <ul>
                            <li>Certification of the Agency Head that demotion is not a result of an administrative case</li>
                            <li>Written consent by the employee interposing no object to the demotion</li>
                        </ul>
                    </td>
                    <td class="p-1 text-center">{{@$appointment->ar_05}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <label class="font-weight-bold card-title">FOR CSCFO ACTION</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td class="p-1 text-left">
                        <p class="mb-6">Agency</p>
                        <p class="mb-4">Action</p>
                    </td>
                    <td class="p-1 text-left">
                        <p class="mb-6">( ) Regulated</p>
                        <p class="mb-3">( ) Approved</p>
                        <p class="mb-3">( ) Disapproved</p>
                    </td>
                    <td class="p-1 text-left" style="width: 60em;">
                        <p class="mb-3">
                            ( ) Appointment submitted to CSCFO within 30 calendar days from date of issuance.
                        </p>
                        <p>
                            ( ) Appointment submitted to CSCFO beyond 30 calendar days from date of issuance.
                        </p>
                        <p style="text-indent: 20px;" class="mb-3">Effective:______________</p>
                        <p class="mb-3">Ground/s for Invalidation</p>
                    </td>
                </tr>
                <tr>
                    <td class="text-left p-1">
                        <p class="mb-8">Evaluated By:</p>
                    </td>
                    <td class="text-left p-1">
                        <p class="mb-8">Verified By:</p>
                    </td>
                    <td class="text-left p-1">
                        <p class="mb-8">Final Action By:</p>
                    </td>
                </tr>
                <tr>
                    <td class="text-left p-1">Date:</td>
                    <td class="text-left p-1">Date:</td>
                    <td class="text-left p-1">Date:</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection