@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit Processing Checklis</h2>
    </div>

    <!-- Job Offer Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit an appointment processing in the form below.</span></div>
                <div class="card-body">
                    @include('appointment-processing._form', [
                        'action' => ['AppointmentProcessingController@update', $processing->id],
                        'method' => 'PATCH',
                        'processing' => $processing,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
