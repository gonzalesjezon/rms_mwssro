@section('css')
     <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {!! Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) !!}
            <div class="col-12 col-sm-8 col-lg-6">
                @if(@$posDesc)
                {!! Form::label('', $posDesc->applicant->getFullName(), ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) !!}
                <input type="hidden" name="applicant_id" value="{{$posDesc->applicant_id}}">
                @else
                <select class="form-control-sm form-control-xs" name="applicant_id" id="applicant_id" style="width: 100% !important">
                    <option value="">Select Applicant</option>
                    @foreach(@$boardings as $board)
                    <option value="{{ @$board->applicant_id }}" data-position="{{ @$board->applicant->job->plantilla_item->position->Name }}" data-office="{{ @$board->applicant->job->plantilla_item->office->Name }}" data-salary_grade="{{ @$board->applicant->job->plantilla_item->salary_grade->Name }}" data-item_number="{{ @$board->applicant->job->plantilla_item->Name }}" data-basic_salary="{{ @$board->applicant->job->monthly_basic_salary }}" data-division="{{ @$board->applicant->job->plantilla_item->division->Name }}">{!! @$board->applicant->getFullName() !!}</option>
                    @endforeach
                </select>
                @endif
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$posDesc->applicant->job->plantilla_item->position->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'position'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Item Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$posDesc->applicant->job->plantilla_item->item_number, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'item_number'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Job / Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$posDesc->applicant->job->plantilla_item->salary_grade->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'salary_grade'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Monthly Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', number_format(@$posDesc->applicant->job->plantilla_item->basic_salary,2), [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'basic_salary'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <b>13. Position Title</b>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Immediate Supervisor', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('immediate_supervisor', @$posDesc->immediate_supervisor, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <b>15. Position Title, and Item of those Directly Supervised :</b>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Item Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('item_number', @$posDesc->item_number, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
           &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$posDesc->applicant->job->plantilla_item->office->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'office'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$posDesc->applicant->job->plantilla_item->division->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'division'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Place of Work', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Next Higher Supervisor', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('higher_supervisor', @$posDesc->higher_supervisor, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('position_title', @$posDesc->position_title, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('used_tools', 'MACHINE, EQUIPMENT, TOOLS, ETC., USED REGULARLY IN PERFORMANCE OF WORK:', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('used_tools') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="used_tools" name="used_tools"></div>
                {{ Form::textarea('used_tools', '',['id'=>'used_tools-text', 'class'=>'d-none']) }}
                {!! $errors->first('used_tools', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6 text-center font-weight-bold">Internal</div>
    <div class="col-6 text-center font-weight-bold">External</div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Executive/Managerial', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="managerial">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->managerial == $key ) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Supervisors', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="supervisor">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->supervisor == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Non Supervisors', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="non_supervisor">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->non_supervisor == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Staff', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="staff">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->staff == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::label('', 'Working Condition', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office Work', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="office_work">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->office_work == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Field Work', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="field_work">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->field_work == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'General Public', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="general_public">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->general_public == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other Agencies', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="other_agency">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posDesc->other_agency == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other/s (Please Specify)', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('other_contacts', @$posDesc->other_contacts, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other/s (Please Specify)', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('other_condition', @$posDesc->other_condition, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('description_function_unit', 'BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE UNIT OR SECTION:', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('description_function_unit') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="description_function_unit" name="description_function_unit"></div>
                <textarea name="description_function_unit" class="d-none" id="description_function_unit-text"></textarea>
                <!-- <input type="hidden" name="description_function_unit" id="description_function_unit-text"> -->
                {!! $errors->first('description_function_unit', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('description_function_position', 'BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE POSITION (Job Summary):', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('description_function_position') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="description_function_position" name="description_function_position"></div>
                <textarea name="description_function_position" class="d-none" id="description_function_position-text"></textarea>
                <!-- <input type="hidden" name="description_function_position" id="description_function_position-text"> -->
                {!! $errors->first('description_function_position', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        {{ Form::label('', 'Qualification Standards', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>

<div class="row">
    <div class="col-3 text-center">
        <b>Education</b>
    </div>
    <div class="col-3 text-center">
        <b>Experience</b>
    </div>
    <div class="col-3 text-center">
        <b>Training</b>
    </div>
    <div class="col-3 text-center">
        <b>Eligibility</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-3">
        <div id="applicant_education"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_experience"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_training"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_eligibility"></div>
        <textarea class="d-none"></textarea>
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Core Compentencies</b>
    </div>
    <div class="col-6 text-center">
        <b>Compentecy Level</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div id="core_compentency"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-6">
        <div id="compentency_1" name="compentency_1"></div>
        {{ Form::textarea('compentency_1', '',['id'=>'compentency_1-text', 'class'=>'d-none']) }}
        {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Leader Compentencies</b>
    </div>
    <div class="col-6 text-center">
        <b>Compentecy Level</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div id="leader_compentency"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-6">
        <div id="compentency_2" name="compentency_2"></div>
        {{ Form::textarea('compentency_2', '',['id'=>'compentency_2-text', 'class'=>'d-none']) }}
        {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        {{ Form::label('', 'STATEMENT OF DUTIES AND RESPONSIBILITIES (Technical Competencies):', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Percentage of Worktime</b>
    </div>
    <div class="col-6 text-center">
        <b>Duties and Responsibilities</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6 ">
        {{ Form::text('percentage_work_time', @$posDesc->percentage_work_time, [
            'class' => 'form-control form-control-sm col-sm-6 offset-3',
        ])
        }}
    </div>

    <div class="col-6">
        <div id="responsibilities" name="responsibilities"></div>
        {{ Form::textarea('responsibilities', '',['id'=>'responsibilities-text', 'class'=>'d-none']) }}
        {!! $errors->first('responsibilities', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Supervisor Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('supervisor_name', @$posDesc->supervisor_name, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="id" value="{{@$posDesc->id}}">
    
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        let data = {
          '#used_tools': `{!! @$posDesc->used_tools !!}`,
          '#description_function_unit': `{!! @$posDesc->description_function_unit !!}`,
          '#description_function_position': `{!! @$posDesc->description_function_position !!}`,
          '#applicant_education': ``,
          '#applicant_experience': ``,
          '#applicant_training': ``,
          '#applicant_eligibility': ``,
          '#core_compentency': `{!! @$posDesc->applicant->job->compentency_1 !!}`,
          '#leader_compentency': `{!! @$posDesc->applicant->job->compentency_2 !!}`,
          '#compentency_1': `{!! @$posDesc->compentency_1 !!}`,
          '#compentency_2': `{!! @$posDesc->compentency_2 !!}`,
          '#responsibilities': `{!! @$posDesc->responsibilities !!}`,
        };

        // initialize editors for each data element
        App.textEditors(Object.keys(data));

        // when validation fails, get data from hidden input texts
        // and set as value for wysiwyg editor
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            if (data[key] === '') {
              data[key] = $(`${key}-text`).html();
            }
            setData(key, data[key]);
          }
        }

        // set data for wysiwyg editors
        function setData(selector = '', data = '') {
          $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
        }

        $('.note-toolbar').remove();
        // on form submit, get data from wysiwyg editors
        // pass to hidden input elements
        $('#appointment-submit').click(function() {
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              let element = getData(key);
              $(`${key}-text`).val(element);
            }
          }
        });

        function getData(selector = '') {
          let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
          return data.html();
        }

        $('#applicant_id').change(function(){
            position    = $(this).find(':selected').data('position');
            office      = $(this).find(':selected').data('office');
            division    = $(this).find(':selected').data('division');
            itemNumber  = $(this).find(':selected').data('item_number');
            salaryGrade = $(this).find(':selected').data('salary_grade');
            basicSalary = $(this).find(':selected').data('basic_salary');

            $("#position").val(position);
            $("#office").val(office);
            $("#item_number").val(itemNumber);
            $("#salary_grade").val(salaryGrade);
            $("#basic_salary").val(basicSalary);
            $("#division").val(division);

        });


    });
    </script>
@endsection
