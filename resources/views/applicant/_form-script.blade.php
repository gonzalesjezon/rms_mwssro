<!-- JS Libraries -->
<script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-booking.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    App.wizard();

    // run the parsely validation before changing forms
    $('.wizard-next, .wizard-previous').click(function() {
      tabs.validate(this, 8);
    });

    let tabs = {
      validate: function(element, noOfTabs) {
        // validate each element in current form tab
        var noError = true;
        var result = true;
        $(element).closest('[data-step]').find('input, textarea, select').each(function() {
          $(this).parsley().validate();
          result = $(this).parsley().isValid();
          if (!result) {
            noError = result;
          }
        });

        // set css class active and complete tabs
        let step = $(element).closest('[data-step]').data('step');
        if (!noError) {
          for (i = 1; i <= noOfTabs; i++) {
            if (i != step) {
              $(`[data-step*="${i}"]`).removeClass('active complete');
            }
            else {
              $(`[data-step*="${i}"]`).addClass('active complete');
            }
          }
        }
      },
    };

    let vocational_ctr = 0;
    $('#add_vocational').click(function(e) {
      vocational_ctr += 1;
      let vocationalHtml = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="vocational[${vocational_ctr}][awards]" type="text" style="display:inline-block">` +
            `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(vocationalHtml).insertAfter('.vocational');
      e.preventDefault();
    });

    let workexperience_ctr = 0;
    $('#add_workexperience').click(function(e) {
      workexperience_ctr += 1;
      let workexperienceHtml = '' +
        `<div class="row mt-2">` +
        `<div class="col-2 text-center">` +
        `<div class="row">` +
            `<div class="col-6 pr-1">` +
            `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][inclusive_date_from]" type="text">` +
            `</div>` +
            `<div class="col-6 pl-1">` +
            `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][inclusive_date_to]" type="text">` +
            `</div>` +
        `</div>` +
        `</div>` +
        `<div class="col-2 pr-1 pl-0 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][position_title]" type="text">` +
        `</div>` +
        `<div class="col-3 pr-0 pl-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][department]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][monthly_salary]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][salary_grade]" type="text">` +
        `</div>` +
        `<div class="col-2 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][status_of_appointment]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="work_experience[${workexperience_ctr}][govt_service]" type="text" style="display:inline-block">` +
            `<button class="btn btn-danger col-5 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(workexperienceHtml).insertAfter('.work_experience');
      e.preventDefault();
    });

    let eligibility_ctr = 0;
    $('#add_eligibility').click(function(e) {
      eligibility_ctr += 1;
      let eligibilityHtml = '' +
        `<div class="row mt-2">` +
        `<div class="col-3">` +
        `{{ Form::select('eligibility[${eligibility_ctr}][eligibility_ref]', config('params.eligibility_type'), '', [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select Eligibility',
                'required' => true,
            ])
        }}`+
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][rating]" type="text">` +
        `</div>` +
        `<div class="col-2 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][exam_date]" type="text">` +
        `</div>` +
        `<div class="col-2 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][exam_place]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][license_number]" type="text">` +
        `</div>` +
        `<div class="col-2 text-center">` +
        `<input class="form-control form-control-sm col-8 pr-0 mr-1" name="eligibility[${eligibility_ctr}][license_validity]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(eligibilityHtml).insertAfter('.eligibility');
      e.preventDefault();
    });


    let college_ctr = $('#college_ctr').val();
    $('#add_college').click(function(e) {
      college_ctr = Number(college_ctr) + 1;
      let collegeHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="college[${college_ctr}][awards]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(collegeHTML).insertAfter('.college');
      e.preventDefault();
    });

    let graduate_studies_ctr = $('#graduate_ctr').val();
    $('#add_graduate_studies').click(function(e) {
      graduate_studies_ctr = Number(graduate_studies_ctr) + 1;
      let graduateStudiesHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-3">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][attendance_from]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][attendance_to]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="graduate[${graduate_studies_ctr}][awards]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(graduateStudiesHTML).insertAfter('.graduate-studies');
      e.preventDefault();
    });

    let training_ctr = $('#training_ctr').val();
    $('#add_training').click(function(e) {
      training_ctr = Number(training_ctr) + 1;
      let trainingHtml = '' +
        `<div class="row mt-2">` +
        `<div class="col-3 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][title_learning_programs]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][inclusive_date_from]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][inclusive_date_to]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][number_hours]" type="text" required>` +
        `</div>` +
        `<div class="col-3 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][ld_type]" type="text">` +
        `</div>` +
        `<div class="col-3 pl-1 text-center">` +
        `<input class="form-control form-control-sm col-9 pr-0 mr-1" name="training[${training_ctr}][sponsored_by]" type="text" style="display:inline-block">` +
            `<button class="btn btn-danger col-2 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(trainingHtml).insertAfter('.training');
      e.preventDefault();
    });

    $('#swt6').change(function(){

        let houseNumber = $('#house_number').val();
        let street      = $('#street').val();
        let subdivision = $('#subdivision').val();
        let barangay = $('#barangay').val();
        let city = $("#city").val();
        let province = $('#province').val();

        if($(this).is(':checked')){
            $('#permanent_house_number').val(houseNumber);
            $('#permanent_street').val(street);
            $('#permanent_subdivision').val(subdivision);
            $('#permanent_barangay').val(barangay);
            $('#permanent_city').val(city);
            $('#permanent_province').val(province);
        }else{
            $('.clear-form-input').val('');
        }
    });

    let applicant_id = `{{ $applicant->id }}`;

    $(document).on('click','.remove',function(){
        id            = $(this).data('id');
        level         = $(this).data('level');
        base_url = `{{ url('applicant') }}`;

        if(id){
            swal({
                title: "Confirm Delete?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
            }).then(function(isConfirm){
                if(isConfirm.value == true){
                    $.ajax({
                        url:base_url+'/delete',
                        data:{
                            '_token':`{{ csrf_token() }}`,
                            'id':id,
                            'level':level,
                        },
                        type:'POST',
                        dataType:'JSON',
                        success:function(result){
                            swal({
                              title: 'Deleted Successfully',
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "OK",
                              closeOnConfirm: false
                            });

                            window.location.href = base_url+'/'+applicant_id+'/edit';
                        }
                    })
                }else{
                    return false;
                }
            });
        }else{
            $(this).closest('.row').remove();
        }

    })

     $('#select_qualified').change(function(){
        val = $(this).find(':selected').val();
        $('#selected_qualified').val(val);
    })

    $('#select_qualified').trigger('change');

    $('.btn-save').click(function(){
        $('#save_applicant').trigger('click');
    })

    $('#send_mail').click(function(){
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {

            var arr = {};
            $("[class*='form-control']").each(function () {
                var obj_name = $(this).attr("name");
                var value    = $(this).val();
                arr[obj_name] = value;
            });

            $.ajax({
                url:`{{ url('applicant/sendMail') }}`,
                data:{
                    'data':arr,
                    '_token':`{{ csrf_token() }}`
                },
                type:'POST',
                dataType:'JSON',
                success:function(result){
                    Swal.fire(
                      'Sent Successfully!',
                      'Your mail has been sent.',
                      'success'
                    )

                }
            })
          }

        });

    });

  });
</script>