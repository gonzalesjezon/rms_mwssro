@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action,'method' => $method,'id' => 'applicant-form-one',
            'data-parsley-namespace' => "data-parsley-", 'data-parsley-validate' => '',
            'class' => 'form-horizontal group-border-dashed'
        ]) !!}

<input type="hidden" name="id" value="{{@$psipop->id}}">

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('item_number', @$psipop->item_number, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>


        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="position_id">
                    <option value="0">Select position</option>
                    @foreach($positions as $position)
                    <option value="{{ $position->RefId }}" {{ ($position->RefId == @$psipop->position_id) ? 'selected' : '' }}>{!! $position->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="employee_status_id">
                    <option value="0">Select employee status</option>
                    @foreach($employee_status as $status)
                    <option value="{{ $status->RefId }}" {{ ($status->RefId == @$psipop->employee_status_id) ? 'selected' : '' }}>{!! $status->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="office_id">
                    <option value="0">Select office</option>
                    @foreach($offices as $office)
                    <option value="{{ $office->RefId }}" {{ ($office->RefId == @$psipop->office_id) ? 'selected' : '' }} >{!! $office->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="division_id">
                    <option value="0">Select division</option>
                    @foreach($divisions as $division)
                    <option value="{{ $division->RefId }}" {{ ($division->RefId == @$psipop->division_id) ? 'selected' : '' }}>{!! $division->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('department_id') ? 'has-error' : ''}}">
            {{ Form::label('', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="department_id">
                    <option value="0">Select department</option>
                    @foreach($departments as $department)
                    <option value="{{ $department->RefId }}" {{ ($department->RefId == @$psipop->department_id) ? 'selected' : '' }}>{!! $department->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

       <!--  <div class="form-group row {{ $errors->has('ppa_attribution') ? 'has-error' : ''}}">
            {{ Form::label('ppa_attribution', 'PPA Attribution', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('ppa_attribution', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div> -->

        <div class="form-group row {{ $errors->has('salary_grade_id') ? 'has-error' : ''}}">
            {{ Form::label('', 'Salary Grade', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="salary_grade_id">
                    <option value="0">Select salary grade</option>
                    @foreach($salary_grades as $salary_grade)
                    <option value="{{ $salary_grade->RefId }}" {{ ($salary_grade->RefId == @$psipop->salary_grade_id) ? 'selected' : '' }}>{!! $salary_grade->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row {{ $errors->has('step_id') ? 'has-error' : ''}}">
            {{ Form::label('', 'Step', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="step_id">
                    <option value="0">Select step</option>
                    @foreach($steps as $step)
                    <option value="{{ $step->RefId }}" {{ ($step->RefId == @$psipop->step_id) ? 'selected' : '' }}>{!! $step->Name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row {{ $errors->has('basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('', 'Basic Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('basic_salary', number_format(@$psipop->basic_salary,2), [
                    'class' => 'form-control form-control-sm text-right',
                    'placeholder' => '0.00'
                ]) }}
            </div>
        </div>
    </div>
</div>

<hr>


<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Name of Incumbent', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('incumbent_name', @$psipop->incumbent_name, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Mode of Separation', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('mode_of_separation', @$psipop->mode_of_separation, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

    </div>

    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Date Vacated', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                    <input size="16" type="text" value="{{ @$psipop->date_vacated }}" name="date_vacated"
                           class="form-control form-control-sm">
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
    </div>
</div>

{{ Form::hidden('_token',csrf_token())}}
{!! Form::close() !!}

@section('scripts')
    @include('psipop._form-script')
@endsection
