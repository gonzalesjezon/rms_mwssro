<div class="be-content">
    <div class="main-content container-fluid">
        @include('layouts._flash-message')
        @yield('content')
    </div>
</div>
