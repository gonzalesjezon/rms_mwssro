@extends('layouts.app')
@section('content')
<div class="be-wrapper be-login">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container forgot-password">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        <img src="{{ URL::asset('beagle-assets/img/logo-xx.png') }}" alt="logo" width="102" height="27" class="logo-img">
                        <span class="splash-description">Forgot your password?</span>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                            <p>Don't worry, we'll send you an email to reset your password.</p>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} pt-4">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your Email" autocomplete="off" required>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <p class="pt-1 pb-4">Don't remember your email? <a href="#">Contact Support</a>.</p>
                            <div class="form-group pt-1">
                                <button type="submit" class="btn btn-block btn-primary btn-xl">Send Password Reset Link</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="splash-footer">&copy; 2016 Your Company</div>
            </div>
        </div>
    </div>
</div>
@endsection