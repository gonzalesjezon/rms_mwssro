@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Schedule of Interviews</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <div class="btn-group btn-space">
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                <i class="icon icon-left mdi mdi-account-add"></i> Add <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
              <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 30px, 0px);">
                <a class="dropdown-item" href="{{ route('interviews.create',['status' => 'plantilla']) }}">Plantilla</a>
                <a class="dropdown-item" href="{{ route('interviews.create',['status' => 'nonplantilla']) }}">Non Plantilla</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Applicant Name</th>
                <th>Position Applied</th>
                <th>Date & Time</th>
                <th>Location</th>
                <th>Confirmation</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
                 @foreach($interviews as $interview)
                  <tr>
                    <td class="actions text-left">
                      <div class="tools">
                        <button type="button" data-toggle="dropdown"
                                class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                          <i class="icon icon-left mdi mdi-settings-square"> </i> Options
                          <span class="icon-dropdown mdi mdi-chevron-down"> </span>
                        </button>

                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                          <a href="{{ route('interviews.edit', $interview->id) }}"
                               class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                            <div class="dropdown-divider"></div>

                          {!! Form::open([
                              'method'=>'DELETE',
                              'url' => ['interviews', $interview->id],
                              'style' => 'display:inline'
                          ]) !!}
                          {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', [
                              'type' => 'submit',
                              'style' => 'color: #504e4e',
                              'class' => 'dropdown-item',
                              'title' => 'Delete Record',
                              'onclick'=>'return confirm("Confirm delete?")'
                          ])!!}
                          {!! Form::close() !!}

                        </div>
                      </div>
                    </td>
                    <td>{{ $interview->applicant->getFullName()}}</td>
                    <td>{{ $interview->applicant->job->plantilla_item->position->Name }}</td>
                    <td>{{ $interview->interview_date }} {{$interview->interview_time}}</td>
                    <td>{{ $interview->interview_location }}</td>
                    <td>{{ ($interview->confirmed == 1) ? 'Yes' : 'No' }}</td>
                    <td>{{ config('params.interview_status.'.$interview->interview_status)}}</td>
                  </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();
    });
  </script>
@endsection
