@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => 'JobsController@typePersonnel', 'method'=>'GET', 'id'=>'form_personnel']) !!}
<div class="row">
    <div class="col-6">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Type of Personnel</label>
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" name="type_personnel" id="type_personnel" {{ (@$job->id) ? 'disabled' : '' }}>
                    <option>Select type</option>
                    <option value="plantilla" {{ ($status == 'plantilla') ? 'selected'  : '' }}>Plantilla</option>
                    <option value="nonplantilla" {{ ($status == 'nonplantilla') ? 'selected'  : '' }}>Nonplantilla</option>
                </select>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'get-form']) !!}

<div class="row">
    <div class="col-6">

        <div class="form-group row {{ $errors->has('plantilla_item_id') ? 'has-error' : ''}}">
            {{ Form::label('', 'Plantilla Item No.', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs" id="plantilla_item_id" name="plantilla_item_id" {{ (@$job->id) ? 'disabled' : '' }}>
                    <option value="0">Select </option>
                    @foreach($items as $item)
                    <option value="{{ @$item->id }}" data-position="{{ @$item->position->Name }}" data-office="{{ @$item->office->Name }}" data-division="{{ @$item->division->Name }}" data-division="{{ @$item->division->Name }}" data-salary_grade="{{ @$item->salary_grade->Name }}" data-basic_salary="{{ number_format(@$item->basic_salary,2) }}" data-annual_salary="{{ number_format(@$item->basic_salary * 12,2) }}" {{ ( @$item->id == @$job->plantilla_item_id) ? 'selected' : '' }} >{!! @$item->item_number !!}</option>
                    @endforeach
                </select>
                {!! $errors->first('plantilla_item_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title.', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true,
                    'id' => 'position'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '',[
                        'class' => 'form-control form-control-xs',
                        'readOnly' => true,
                        'id' => 'office'

                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '',[
                        'class' => 'form-control form-control-xs',
                        'readOnly' => true,
                        'id' => 'division'
                    ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ ($status != 'plantilla') ? 'd-none' : '' }} ">
            {{ Form::label('', 'Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true,
                    'id' => 'salary_grade'
                ])
                }}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('monthly_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('monthly_basic_salary', ($status == 'plantilla') ? 'Monthly Basic Salary' : 'Professional Fees', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text"  id="basic_salary"  class="form-control form-control-sm" placeholder="PHP 0" readonly>
            </div>
        </div>

        <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}} {{ ($status != 'plantilla') ? 'd-none' : '' }}">
            {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0',
                    'readOnly' => true,
                    'id' => 'annual_salary'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Place of Assignment', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$job->place_of_assignment,[
                        'class' => 'form-control form-control-sm',
                        'id' => 'get_assignment'
                    ])
                }}
            </div>
        </div>

    </div>
</div>

<hr>

<input type="hidden" name="status" value="{{ $status }}">

 @if($status == 'plantilla')

<div class="row">
    <div class="col-6">

        <div class="form-group row ">
            {{ Form::label('', 'Additional Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
        </div>

        <div class="form-group row {{ $errors->has('pera_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'PERA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('pera_amount', number_format(2000,2),[
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('pera_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('clothing_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Clothing Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('clothing_amount', number_format(6000,2),[
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('clothing_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('midyear_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Mid-Year Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('midyear_amount', '',[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => 'PHP 0',
                    'readonly' => 'true',
                    'id' => 'mid_year'
                ]) }}
            </div>
            {!! $errors->first('midyear_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

    </div>


    <div class="col-6">

        <div class="form-group row {{ $errors->has('yearend_amount') ? 'has-error' : '' }} mt-8">
            {{ Form::label('', 'Year-End Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('yearend_amount', '',[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => 'PHP 0',
                    'readonly' => 'true',
                    'id' => 'year_end'
                ]) }}
            </div>
            {!! $errors->first('yearend_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('cashgift_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Cash Gift', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('cashgift_amount', @$job->cashgift_amount,[
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('cashgift_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
</div>

@else


<div class="row">
    <div class="col-6">
      <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label"> Duration </label>
        <div class="col-12 col-sm-8 col-lg-6">
            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                <input size="16" type="text" value="{{ @$job->duration_from }}" name="duration_from"
                       class="form-control form-control-sm">
                <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
      <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label"> To </label>
        <div class="col-12 col-sm-8 col-lg-6">
            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                <input size="16" type="text" value="{{ @$job->duration_to }}" name="duration_to"
                       class="form-control form-control-sm">
                <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

@endif

<hr>

<div class="form-group row ">
    {{ Form::label('', 'Competency:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>


<div class="row ">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_1') ? 'has-error' : ''}}">
            {{ Form::label('compentency_1', 'Core Competency', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-9">
                <div id="compentency_1" name="compentency_1"></div>
                <textarea name="compentency_1" class="d-none"></textarea>
                <input type="hidden" name="compentency_1" id="compentency_1-text">
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_2') ? 'has-error' : ''}}">
            {{ Form::label('compentency_2', 'Functional Competency', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_2" name="compentency_2"></div>
                <textarea name="compentency_2" class="d-none"></textarea>
                <input type="hidden" name="compentency_2" id="compentency_2-text">
                {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_3') ? 'has-error' : ''}}">
            {{ Form::label('', ($status == 'plantilla') ? 'Leadership Competency' : 'Job Summary', ['class'=>'col-3 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_3" name="compentency_3"></div>
                {{ Form::textarea('compentency_3', '',['id'=>'compentency_3-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_3', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

    </div>

    <!-- <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_4') ? 'has-error' : ''}}"  >
            <span class="col-12 col-sm-4 col-form-label text-sm-right"> {{ ($status == 'plantilla') ? 'Specific Duties & Responsibilities' : 'Auxilliary Functions' }} </span>
            <div class="col-8">
                <div id="compentency_4" name="compentency_4"></div>
                {{ Form::textarea('compentency_4', '',['id'=>'compentency_4-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_4', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div> -->

    <div class="col-6 {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        <div class="form-group row {{ $errors->has('compentency_5') ? 'has-error' : ''}}">
            {{ Form::label('', 'Secondary Functions', ['class'=>'col-3 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_5" name="compentency_5"></div>
                {{ Form::textarea('compentency_5', '',['id'=>'compentency_5-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_5', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="form-group row ">
    {{ Form::label('', 'Required Qualifications:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
            {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="education" name="education"></div>
                {{ Form::textarea('education', @$job->education,['id'=>'education-text', 'class'=>'d-none']) }}
                {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
            {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="training" name="training"></div>
                {{ Form::textarea('training', @$job->training,['id'=>'training-text', 'class'=>'d-none']) }}
                {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
            {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="experience" name="experience"></div>
                {{ Form::textarea('experience', @$job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
                {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
            {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="eligibility" name="eligibility"></div>
                {{ Form::textarea('eligibility', @$job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
                {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-5">
        <div class="form-group row">
            <label class="col-12 col-sm-4 col-form-label text-sm-right font-weight-bold">Publish</label>
            <div class="col-12 col-sm-8 col-lg-6 pt-1">
                <div class="switch-button switch-button-success switch-button-yesno">
                    <input type="checkbox" name="publish" id="swt8" {{ @$job->publish ? 'checked' : '' }}><span>
                                        <label for="swt8"></label></span>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-12 col-sm-4 col-form-label text-sm-right"> Deadline Date </label>
            <div class="col-12 col-sm-6">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                    <input size="16" type="text" value="{{ @$job->deadline_date }}" name="deadline_date"
                           class="form-control form-control-sm">
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-12 col-sm-4 col-form-label text-sm-right"> Application address to: </label>
            <div class="col-12 col-sm-6">
                <select class="form-control form-control-xs" name="appointer_id">
                    @foreach($employeeinfo as $info)
                        @if(!empty($info->employee))
                        <option value="{{ $info->RefId }}" {{ ($info->RefId == @$job->appointer_id) ? 'selected' : '' }}>{!! $info->employee->getFullName() !!} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-7">

        <div class="form-group row">
            {{ Form::label('', 'Publications', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
            <div class="col-10">
                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Agency Website</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_1" id="swt9" {{ @$job->publication_1 ? 'checked' : '' }}><span>
                                                <label for="swt9"></label></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Newspaper</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_2" id="swt10" {{ @$job->publication_2 ? 'checked' : '' }}><span>
                                                <label for="swt10"></label></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">CSC Bulletin</label>
                    <div class="col-12 col-sm-8 col-lg-2 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_3" id="swt11" {{ @$job->publication_3 ? 'checked' : '' }}><span>
                                                <label for="swt11"></label></span>
                        </div>
                    </div>
                    <label class="col-12 col-sm-2 col-form-label text-left pl-0">Approved Date</label>
                    <div class="col-4">
                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                            <input size="16" type="text" value="{{ @$job->approved_date }}" name="approved_date"
                                   class="form-control form-control-sm">
                            <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Other</label>
                    <div class="col-12 col-sm-8 col-lg-2 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_4" id="swt12" {{ @$job->publication_4 ? 'checked' : '' }}><span>
                                                <label for="swt12"></label></span>
                        </div>
                    </div>
                    <label class="col-12 col-sm-2 col-form-label text-left pl-0">Pls. Specify</label>
                    <div class="col-4">
                        <input type="text" name="other_specify" id="other_specify" value="{{@$job->other_specify}}" class="form-control form-control-sm" >
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


<hr>
<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('jobs._form-script')
@endsection
