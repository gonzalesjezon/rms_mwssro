@extends('layouts.print')

@section('css')
<style type="text/css">
  .table1>thead>tr>td, .table1>tbody>tr>td{
    border:1px solid #333 !important;
  }
</style>
@endsection

@section('content')

@if($applicant)
<div id="reports" style="width: 960px;margin: auto; font-size: 10pt;font-family: Arial, Helvetica, sans-serif;">
    <div class="report1" style="border: 10px solid #b1acac;padding: 20px;">
      <div class="row mb-1">
        <div class="col-3 font-italic">CS Form No. 33-B <br> Revised 2018</div>
        <div class="col-6"></div>
        <div class="col-3 text-right"><i>(Stamp of Date of Receipt)</i></div>
      </div>

      <div class="row mb-6">
        <div class="col-12 text-center">
            <div style="font-size: 16px;" class="font-weight-bold">Republic of the Philippines</div>
            <div style="font-size: 14px;">METROPOLITAN WATERWOKS AND SEWERAGE SYSTEM - REGULATORY OFFICE</div>
            <div>3rd Floor Engineering Bldg., MWSS Complex,</div>
            <div>Katipunan Road, Balara Quezon City</div>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-12">
            <p class="font-weight-bold">Mr./Mrs./ Ms.: {!! $applicant->applicant->getFullName() !!}  </p>
        </div>
      </div>

      <div class="row">
          <div class="col-3 text-right pr-0">You are hereby appointed as</div>
          <div class="col-7 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! $applicant->applicant->job->plantilla_item->position->Name !!}</div>
          <div class="col-1 text-center pr-0 pl-0">(SG/JG/PG)</div>
          <div class="col-1 border border-dark border-top-0 border-right-0 border-left-0 text-center">{!! $applicant->applicant->job->plantilla_item->salary_grade->Name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-3"></div>
          <div class="col-7 text-center">(Position Titlte)</div>
      </div>

      <div class="row">
          <div class="col-1 text-left pl-3 pr-0">under</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0">{!! config('params.employee_status.'.$applicant->applicant->job->plantilla_item->employee_status_id) !!}</div>
          <div class="col-1 text-center pl-0 pr-0">status at the</div>
          <div class="col-6 border border-dark border-top-0 border-right-0 border-left-0 text-center">{!! $applicant->applicant->job->plantilla_item->division->Name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-6 text-center">(Permanent,Temporary,etc.)</div>
          <div class="col-6 text-center">(Office/Department/Unit)</div>
      </div>

      <div class="row mb-6">
        <div class="col-3 text-left pr-0">
            with compensation rate of
        </div>
        <div class="col-5 border border-dark border-top-0 border-right-0 border-left-0 text-center">
          <p class="m-0 p-0" style="font-size: 12px;">{!! strtoupper($number_in_word) !!}</p>
        </div>
        <div class="pl-0 pr-0">(P</div>
        <div class="col-1 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0">{{ number_format($applicant->applicant->job->plantilla_item->basic_salary,2) }}</div>
        <div class="col-2 pl-0 pr-0 text-left">) pesos per month.</div>
      </div>


      <div class="row">
          <div class="col-3 text-right pr-0">The nature of appointment is</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! config('params.nature_of_appointment.'.$applicant->nature_of_appointment) !!}</div>
          <div class="col-1 text-center pr-0 pl-0">vice</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0">{!! @$applicant->vice !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-10 text-center pr-0 pl-0">(Original, Promotion, etc.)    </div>
      </div>

      <div class="row mb-1">
        <div class="col-1 text-left pr-0">
            who
        </div>
        <div class="col-3 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! @$applicant->who !!}</div>
        <div class="pr-0 pl-0 text-center">with Plantilla Item No.</div>
        <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! $applicant->applicant->job->plantilla_item->item_number !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-5 text-center pr-0 pl-0">(Transferred, Retired, etc.)    </div>
      </div>

      <div class="row mb-4">
          <div class="col-1 pr-0">Page No. </div>
          <div class="col-2 border border-dark border-top-0 border-right-0 border-left-0"></div>
      </div>

      <div class="row mb-8">
          <div class="col-8 text-right pr-0">This appointment shall take effect on the date of signing by the appointing officer/authority.</div>
      </div>

      <div class="row mb-6">
        <div class="col-sm-7"></div>
        <div class="col-sm-5">Very truly yours,</div>
      </div>

      <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 text-center">{!! $applicant->appointing_officer !!}</div>
      </div>

      <div class="row mb-6">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 border-top border-dark text-center"> Appointing Officer/Authority</div>
      </div>


      <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 text-center">{!! $applicant->date_sign !!}</div>
      </div>

      <div class="row mb-1">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 border-top border-dark text-center"> Date of Signing</div>
      </div>

      <div style="height: 30em;"></div>

        <div class="row mb-1">
            <div class="col-sm-7">
                Accredited/Deregulated Pursuant to <br>
                CSC Resolution No. _____, s. _____ <br>
                dated

            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-center">
                <img src="{{ url('img/seal.png') }}" >
            </div>
            <div class="col-sm-8"></div>
        </div>

        <div class="row">
            <div class="col-sm-12 text-right"><i style="font-size: 10px;">(Stamp of Date of Release)</i></div>
        </div>
    </div>

    <div style="page-break-before:always; "></div>
    <br>
<!--     <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-8">
            <div class="col-sm-12 font-weight-bold">CSC ACTION:</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-sm-3 border-top"></div>
        </div>
        <div class="row mb-8">
            <div class="col-1"></div>
            <div class="col-sm-3 text-center font-weight-bold">Authorize Official</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-sm-3 border-top"></div>
        </div>
        <div class="row mb-5">
            <div class="col-1"></div>
            <div class="col-sm-3 text-center font-weight-bold">Date</div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-12 text-right"><i>(Stamp of Date of Release)</i></div>
        </div>
    </div> -->

    <div class="report1" style="border: 15px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center" style="font-size: 14pt;">Certification</div>
        </div>

        <?php 
          $pub1 = ($applicant->applicant->job->publication_1) ? 'Agency Web Site' : '';
          $pub2 = ($applicant->applicant->job->publication_3) ? '/ CSC Bulletin of Vacant Position': '';
        ?>

        <div class="row mb-4">
          <div class="col-12">
            <p class="p-0 mb-3 pl-2" style="text-indent: 40px;" class="text-justify">
              This is to certify that all requirements and supporting papers pursuant to <b>CSC MC No. 24, s. 2017</b>,
              <b>as amended,</b> have been complied with, reviewed and found to be in order.
            </p>

            <p class="p-0 mb-3 pl-2" style="text-indent: 40px;" class="text-justify">
              The position was published at <b> <u>{!! $pub1.''.$pub2 !!}</u> </b> from <b><u>{!! date('d F Y', strtotime(@$applicant->publication_date_from)) !!}</u></b> to <b><u>{!! date('d F Y', strtotime(@$applicant->publication_date_to)) !!}</u></b>, and posted in <b><u>{!! $applicant->posted_in !!}</u></b> from <b><u>{!! date('d F Y', strtotime(@$applicant->applicant->job->created_at)) !!}</u></b> to <b><u>{!! date('d F Y', strtotime(@$applicant->applicant->job->deadline_date)) !!}</u></b>, in consonance with RA No. 7041. The assessment by the Human Resource Merit Promotion and Selection Board (HRMPSB) started on <b><u>{!! date('d F Y', strtotime(@$applicant->assessment_date)) !!}</u></b>.
            </p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 text-center">{!! $applicant->hrmo !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top border-dark text-center font-weight-bold">HRMO</div>
          </div>
    </div>

    <div class="report1" style="border: 15px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center" style="font-size: 14pt;">Certification</div>
        </div>

        <div class="row mb-4">
          <div class="col-12 text-justify" style="text-indent: 40px;">
            <p class="pl-2">This      is     to       certify      that       the      appointee      has     been     screened     and     found     qualified by the majority of the HRMPSB during the deliberation held on <br> <b><u>{!! date('d F Y',strtotime($applicant->chairperson_deliberation_date)) !!}</u></b>.</p>
          </div>
        </div>


        <div class="row">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 text-center"> {!! $applicant->chairperson !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top text-center border-dark"> Chairperson, HRMPSB</div>
          </div>
    </div>
    <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center"><h4>CSC Notation</h4></div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <table class="table1 table-bordered col-12">
                    <thead>
                        <tr>
                            <td colspan="3" class="text-center">ACTION ON APPOINTMENTS</td>
                            <td>Recorded By</td>
                        </tr>
                        <tr>
                            <td colspan="3"><input type="checkbox">  Validated per RAI for tde montd of   </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"><input type="checkbox"> Invalidated per CSCRO/FO letter dated    </td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                         <tr >
                            <td><input type="checkbox"> Appeal</td>
                            <td class="text-center">DATE FILED</td>
                            <td class="text-center">STATUS</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> CSCRO/ CSC-Commission </p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr >
                            <td><input type="checkbox"> Petition for Review</td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> CSC-Commission  </p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> Court of Appeals</p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> Supreme Court</p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
    <br>
<!--     <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

        <div class="row mb-6"></div>
        <div class="row mb-4">
            <div class="col-sm-12">
                <p style="text-indent: 50px;" class="text-justify">ANY ERASURE OR ALTERATION ON THE CSC ACTION SHALL NULLIFY OR INVALIDATE THIS APPOINTMENT EXCEPT IF THE ALTERATION WAS AUTHORIZED BY THE COMMISSION. </p>
            </div>
        </div>
    </div> -->
    <br>
    <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

        <div class="row mb-1">
            <div class="col-sm-6">
                <div class="mb-6"></div>
                <ul style="list-style: none;">
                    <li>Original Copy   -   for the Appointee</li>
                    <li>Original Copy   -   for the Civil Service Commission</li>
                    <li>Original Copy   -   for the Agency</li>
                </ul>
            </div>
            <div class="col-sm-6 text-center">
                <p class="font-weight-bold">Acknowledgement</p>
                <p class="font-italic">Received original/photocopy of appointment on_____________</p>
            </div>
        </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top text-center border-dark"> Highest Ranking HRMO</div>
          </div>
    </div>

</div>
@endif
 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection