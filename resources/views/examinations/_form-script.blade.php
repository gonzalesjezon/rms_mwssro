<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#examination-form').parsley(); //frontend validation

    $('#job_id').change(function(){
    	$('#get-form').submit();
    })

    $('#applicant_id').change(function(){
        email = $(this).find(':selected').data('email');
        $('#email').val(email);
    });

    $('#exam_status').change(function(){
      value = $(this).find(':selected').val();

      if(value == 6)
      {
        $('#send_mail').addClass('d-none');
      }
      else
      {
        $('#send_mail').removeClass('d-none');
      }
    });

    $('#send_mail').click(function(){
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {
            var arr = {};
            $("[class*='form-control']").each(function () {
                var obj_name = $(this).attr("name");
                var value    = $(this).val();
                arr[obj_name] = value;
            });

            $.ajax({
                url:`{{ url('examinations/sendEmail') }}`,
                data:{
                    'data':arr,
                    '_token':`{{ csrf_token() }}`
                },
                type:'POST',
                dataType:'JSON',
                success:function(result){
                    Swal.fire(
                      'Sent Successfully!',
                      'Your mail has been sent.',
                      'success'
                    )
                }
            })
          }

        });

    });

  });
</script>