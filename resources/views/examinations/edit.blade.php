@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Schedule of Examination</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can update the schedule of examination in the form below.</span>
                </div>
                <div class="card-body">
                    @include('examinations._form', [
                        'action' => ['ExaminationController@update', $examination->id],
                        'actionPosition' => 'ExaminationController@getApplicant',
                        'method' => 'PATCH',
                        'job' => @$job,
                        'selected' => @$selected,
                        'currentJob' => @$currentJob,
                        'examination' => $examination
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
